var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('users', {
    columns: {
      id: { type: 'string', primaryKey: true },
      token: 'string',
      tokenSecret: 'string',
      lastModified: { type: 'timestamp', defaultValue: new String('current_timestamp') }
    },
    ifNotExists: true
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('users');
};
