var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('AccessLogs', {
    columns: {
      id: { type: 'int', primaryKey: true, autoIncrement: true},
      message: 'string',
      timestamp: { type: 'timestamp', defaultValue: new String('current_timestamp') }
    },
    ifNotExists: true
  }, callback);
};

exports.down = function(db, callback) {
  dp.dropTable('AccessLogs');
};
