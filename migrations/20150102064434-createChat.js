var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('chats', {
    columns: {
      id: { type: 'int', primaryKey: true, autoIncrement: true},
      message: 'string',
      timestamp: { type: 'timestamp', defaultValue: new String('current_timestamp') },
      groupId: 'int'
    },
    ifNotExists: true
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('chats');
};

