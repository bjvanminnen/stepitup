var SVG_NS = 'http://www.w3.org/2000/svg';

var steps = [10000, 10000, 10000, 10000, 8000, 12000, 10000];
var PIXELS_PER = 40;
var days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
var average = 6000;

var days = days.map(function (d, index) {
  var baseDay = (new Date).getDay();
  return days[(baseDay + index + 1) % days.length];
});

var BASE = 600;
var PADDING = 8;
var COL_WIDTH = 70;
var LEFT_BUFFER = 50;

var profiles = {
  'Brent': {
    steps: [14957, 12269, 12186, 12959, 13593, 9464, 292],
    average: 8682
  },
  'Alyssa': {
    steps: [10300, 22153, 5236, 18024, 20700, 15601, 14259],
    average: 12060
  },
  'Josh': {
    steps: [14603, 16511, 9877, 18778, 13521, 19385, 4852],
    average: 14885
  }
};

function createSVGElement(type, options, parent) {
  var element = document.createElementNS(SVG_NS, type);
  for (var key in options) {
    if (key === 'textContent') {
      element.textContent = options[key];
    } else {
      element.setAttribute(key, options[key]);
    }
  }
  if (parent) {
    parent.appendChild(element);
  }
  return element;
}


$(document).ready(function () {
  initSliders()

  var svg = document.getElementById('svg');
  svg.setAttribute('width', 1000); //$(document).width());
  svg.setAttribute('height', 800); //$(document).height());

  var g = createSVGElement('g', {}, svg);

  g.setAttribute('transform', 'translate(' + LEFT_BUFFER + ', 0)');

  var xpos = 0;
  steps.forEach(function (item, index) {
    createSVGElement('rect', {
      id: 'steps' + index,
      fill: 'red',
      opacity: 0.4,
      x: xpos,
      width: COL_WIDTH,
    }, g);

    createSVGElement('text', {
      transform: 'translate(' + xpos + ', ' + (BASE + 20) + ')',
      textContent: days[index]
    }, g);

    // createSVGElement('text', {
    //   transform: 'translate(' + xpos + ', ' + (BASE + 40) + ')',
    //   textContent: 'S: ' + item.toLocaleString()
    // }, g);
    var steps = $("<input>")
      .attr('type', 'text')
      .attr('id', 'stepsNum' + index)
      .css('width', COL_WIDTH + 'px')
      .css('position', 'absolute')
      .css('left', xpos + LEFT_BUFFER)
      .css('top', BASE + 35)
      .val(item.toLocaleString())
      .change(function (event) {
        var newVal = parseInt(event.target.value.replace(/,/g, ''), 10);
        steps[index] = newVal;
        displaySteps();
        displayPoints();
      })
    $('body').append(steps);

    createSVGElement('text', {
      id: 'pointNum' + index,
      transform: 'translate(' + xpos + ', ' + (BASE + 60) + ')',
      textContent: 'P: 0'
    }, g);
    createSVGElement('text', {
      id: 'other' + index,
      transform: 'translate(' + xpos + ', ' + (BASE + 80) + ')',
      textContent: 'P: 0'
    }, g);

    createSVGElement('rect', {
      id: 'points' + index,
      fill: '#0000ff',
      opacity: 0.4,
      x: xpos,
      width: COL_WIDTH
    }, g);

    xpos += COL_WIDTH + 4 * PADDING;
  });

  xpos += 100;
  $("#slider-vertical").css('left', (LEFT_BUFFER + xpos) + 'px');

  createSVGElement('rect', {
    id: 'average',
    fill: 'black',
    height: 3,
    x: -50,
    width: xpos + 50,
  }, g);

  createSVGElement('text', {
    id: 'averageNum',
    x: xpos - 80
  }, g);


  displaySteps();
  displayPoints();
  displayAverage();
});

function initSliders() {
  $("#slider-vertical").slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: BASE * PIXELS_PER,
      value: average,
      slide: function( event, ui ) {
        average = ui.value;
        displayAverage();
        displayPoints();
        // $("#average").val( ui.value );
      }
    });
    // $( "#amount" ).val( $( "#slider-vertical" ).slider( "value" ) );
}

function displaySteps() {
  steps.forEach(function (item, index) {
    var element = document.getElementById('steps' + index);
    var height = item / PIXELS_PER;
    element.setAttribute('height', height);
    element.setAttribute('y', (BASE - height));

    element = document.getElementById('stepsNum' + index);
    element.value = item.toLocaleString();
  });
}

function displayPoints() {
  var totalPoints = 0;
  steps.forEach(function (item, index) {
    var points = calculatePoints(item);
    totalPoints += points;
    var element = document.getElementById('points' + index);
    var height = points / PIXELS_PER;
    element.setAttribute('height', height);
    element.setAttribute('y', (BASE - height));

    var num = document.getElementById('pointNum' + index);
    num.textContent = 'P: ' + Math.round(points).toLocaleString();

    var other = document.getElementById('other' + index);
    other.textContent = Math.round(points / item * 100).toLocaleString() + '%';
  });

  $("#totalPoints").text('Total: ' + Math.round(totalPoints).toLocaleString());
}

function displayAverage() {
  var element = document.getElementById('average');
  var height = average / PIXELS_PER;
  element.setAttribute('y', (BASE - height));

  element = document.getElementById('averageNum');
  element.textContent = average.toLocaleString();
  element.setAttribute('y', (BASE - height));
}

function applyProfile(name) {
  var profile = profiles[name];
  steps = profile.steps.slice();
  average = profile.average;
  displaySteps();
  displayPoints();
  displayAverage();
}

function calculatePoints(dailySteps) {
  var multiplier = 10000 / average;

  var line = average * 0.8;
  if (dailySteps <= line) {
    return multiplier * dailySteps;
  }

  return (line * multiplier) + dailySteps - line;
}

$(document).ready(function () {
  applyProfile('Brent')
});
