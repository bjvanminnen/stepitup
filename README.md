#Getting started
(assumes node/npm already installed):

`npm install` to install dependencies

`node server/app.js` to run server on localhost:5000

If you don't want to install/configure postgres, you can run without a db by setting the NO_DB environment variable `export NO_DB=true`. If you do this, chat will not work.

#Making changes

webpack is the tool used to parse all of the js/jsx files and produce a single output. To run this once you can do `npm run webpack`. To have it run constantly (building changes on save) run `npm run webpack-live`