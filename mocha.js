#!/usr/bin/env node

var Mocha = require('mocha');
var fs = require('fs');
var path = require('path');
var Q = require('q');
Q.longStackSupport = true;

var hideLogs = true;

// Look for grep
var grep = '';
process.argv.forEach(function(arg) {
  var grepTest = /^--grep=(.*)/.exec(arg);
  if (grepTest) {
    grep = grepTest[1];
  }

  if (arg === '--log') {
    hideLogs = false;
  }
});

var mocha = new Mocha({
  grep: grep
});

fs.readdirSync('server/test').filter(function(file){
  // Only keep the .js files
  return file.substr(-3) === '.js';
}).forEach(function(file){
  mocha.addFile(
    path.join('server/test', file)
  );
});

process.env.NODE_ENV = 'test';

if (hideLogs) {
  var logger = require('./server/logger');
  logger.hide();
}

mocha.run(function (failures) {
  process.exit(failures);
});
