var db = require('../db');
var logger = require('../logger');

var TABLE_NAME = 'Chats';

var Chat = function(props) {
  this.id = props.id || null;
  this.message = props.message;
  this.timestamp = props.timestamp || null;

  this.persisted_ = false;
};
module.exports = Chat;

Chat.prototype.save = function (callback) {
  if (process.env.NO_DB) {
    return callback(null);
  }

  var self = this;
  var str;
  callback = callback || function () {};
  if (this.persisted_) {
    str = 'UPDATE ' + TABLE_NAME + ' SET (message, timestamp) = ($$' + this.message +
      '$$, DEFAULT) WHERE id = ' + this.id + 'RETURNING *;';
  } else {
    str = 'INSERT INTO ' + TABLE_NAME + ' (message) VALUES ($$' + this.message + '$$) RETURNING *;';
  }

  db.performQuery(str, function (err, result) {
    if (err) {
      return callback(err, null);
    }
    var row = result.rows[0];
    self.id = row.id;
    self.message = row.message;
    self.timestamp = row.timestamp;
    self.persisted_ = true;
    callback(null, row);

  });

};

// TODO - arguably should have a controller independent from the model
Chat.getMessages = function (req, res) {
  var n = 50;
  if (process.env.NO_DB) {
    return res.send([]);
  }

  var query = 'SELECT * FROM ' + TABLE_NAME + ' ORDER BY timestamp DESC ' +
  'limit ' + n + ';';

  db.performQuery(query, function (err, result) {
    if (err) {
      logger.error("error: " + err);
      res.send("Error: " + err);
    } else {
      res.send(result.rows);
    }
  });
};

Chat.addMessage = function (req, res) {
  var chat = new Chat({
    message: req.body.msg
  });
  chat.save(function (err, result) {
    if (err) {
      logger.error(err);
      res.status(500).send();;
    } else {
      res.status(200).send();
    }
  });
};
