var express = require('express');
var router = new express.Router();
module.exports = router;

var Chat = require('./chat_model');

router.get('/', Chat.getMessages);
router.post('/add', Chat.addMessage);
