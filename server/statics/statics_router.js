var express = require('express');
var router = new express.Router();
module.exports = router;

// todo - debug vs. prod
// todo - ../client feels a little dirty

var clientPath = __dirname + '/../../client/';

router.use('/source.js', express.static(clientPath + 'build/source_dbg.js'));
router.use('/style', express.static(clientPath + 'style'));
router.use('/index.html', express.static(clientPath + 'index.html'));
router.use('/failure.html', express.static(clientPath + 'failure.html'));

router.get('/', function (req, res) {
  res.redirect('/index.html');
});
