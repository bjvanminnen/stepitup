// TODO - reset secrets at some point


var express = require('express');
var session = require('express-session');
var passport = require('passport');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var logger = require('./logger');
var authController = require('./authController');

logger.info('env - ' + (process.env.NODE_ENV || 'prod'));
if (process.env.NODE_ENV === 'test') {
  process.env.DATABASE_URL = process.env.DATABASE_URL || 'postgres://localhost/stepitup_test';
}

// var wrap = require('./wrapHttp');
// wrap.config.showResponse = false;

var app = express();
app.use(cookieParser(process.env.COOKIE_SECRET || 'cookiesecret'));
app.use(session({secret: process.env.SESSION_SECRET || 'supersecret'}));
app.use(passport.initialize());
// TODO - better way to not use for db? also want to restrict more routes
app.use(/\/((?!db).)*/, passport.session());
passport.use('fitbit', authController.fitbitStrategy);

app.use(/\/((?!db).)*/, authController.authenticateFromCookie);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

require('./routes')(app);

var port = process.env.PORT || 5000;
app.listen(port);
logger.info("listening on port " + port);

module.exports = app;
