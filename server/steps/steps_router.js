var express = require('express');
var router = new express.Router();

var logger = require('../logger');
module.exports = router;

var grouper = require('./group_controller');
/**
 * Router to get step data via the non-authenticated API
 */

function handleGroupRequest (req, res, next) {
  var groupId = req.params.groupId;
  var weekStarting = req.params.date;

  grouper.getData(groupId, weekStarting).then(function (result) {
    res.send(result);
  }).fail(function (err) {
    logger.error(err);
    res.status(500).send({error: err.message || err});
  });
};

router.get('/group/:groupId/:date', handleGroupRequest);
