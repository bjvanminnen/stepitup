// TODO - does this belong outside of steps? probably
var Q = require('q');
var http = require('http');
var logger = require('../logger');

var utils = module.exports;


/**
 * @param {string}
 * @returns Q promise
 */
utils.httpRequest = function (url) {
  var defer = Q.defer();

  http.get(url, function (res) {
    var body = '';
    res.on('data', function (chunk) {
      body += chunk;
    });
    res.on('end', function () {
      defer.resolve(body);
    });
  }).on('error', function (e) {
    logger.error("error: " + e);
    defer.reject(e);
  });

  return defer.promise;
};
