var Q = require('q');
var assert = require('assert');
var moment = require('moment');
var utils = require('../steps/utils');
var logger = require('../logger');

var fitbitChartAPI = module.exports;

/**
 * @param {string} userId
 * @param {string} date YYYY-MM-DD
 * @promise {Object<DateId, StepData>}
 */
fitbitChartAPI.getSteps = function (userId, date) {
  var defer = Q.defer();

  // change to format fitbit expects
  var dateTo = moment(date).format('YYYY-MM-DD');
  if (!date || dateTo === 'Invalid date') {
    throw new Error('Invalid Date: ' + date);
  }

  var url = 'http://www.fitbit.com/graph/getGraphData?userId=' + userId +
    '&type=stepsTaken&period=1m&apiFormat=json&dateTo=' + dateTo;

  utils.httpRequest(url).then(function (result) {
    var stepData = {};
    var obj = JSON.parse(result);
    if (obj.result && obj.result.errors) {
      defer.reject(obj.result.errors[0]);
    }

    var dataset = obj.result.chart.datasets[0];
    dataset.forEach(function (dailyInfo) {
      var date = fitbitChartAPI.dateFromDailyInfo_(dailyInfo);
      var steps = fitbitChartAPI.stepsFromDailyInfo_(dailyInfo);
      stepData[date] = { steps: steps };
    });

    defer.resolve(stepData);
  })
  .fail(function (err) {
    logger.error(err.stack);
    defer.reject(err);
  });

  return defer.promise;
};

/**
 * Parse the name returned from our API call to extract a date in the form YYYY-MM-DD
 * @static
 * @param dailyInfo.name {string} String representation of the utc time in ms
 * @returns {string} date in form YYYY-MM-DD
 */
fitbitChartAPI.dateFromDailyInfo_ = function (dailyInfo) {
  assert(dailyInfo.name);
  var asInt = parseInt(dailyInfo.name, 10);
  return moment(asInt).utc().format('YYYY-MM-DD');
};

/**
 * Parse the value/description from our API to extract the number of steps.
 * @static
 * @param dailyInfo.value {number}
 * @param dailyInfo.description {string}
 * @reutrns {number}
 */
fitbitChartAPI.stepsFromDailyInfo_ = function (dailyInfo) {
  assert(dailyInfo.value !== undefined);
  assert(dailyInfo.description);
  var match = /^(\d*) steps taken on/.exec(dailyInfo.description);
  assert(match);
  if (dailyInfo.value === 2000 && match[1] === '0') {
    // some sort of weird case that my testUser hits, likely because it doesnt
    // have a device paired
  } else {
    assert.equal(match[1], dailyInfo.value);
  }
  return dailyInfo.value;
};
