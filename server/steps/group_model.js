var Q = require('q');

var db = require('../db');
var logger = require('../logger');

var TABLE_NAME = 'Groups';

var fakeDB = {
  1: {
    users: ['24PBJ8', '24PHZH', '24D7FK', '26R3HT', '2TRGWL', '2S3RLC', '24C3MB']
  },
  2: {
    users: ['24PBJ8', '24PHZH']
  },
};

var Group = function(props) {
  this.id = props.id || null;
  // TODO right now this is just userId, but eventually user and character
  // will be separate concepts
  this.characters_ = props.characters || [];

  this.persisted_ = false;
};
module.exports = Group;

// TODO - common model class?/interface?
Group.prototype.save = function () {
  var defer = Q.defer();

  throw new Error('NYI');

  return defer.promise;
};

Group.find = function (id) {
  var defer = Q.defer();

  var entry = fakeDB[id];
  if (entry) {
    var dbGroup = new Group({
      id: id,
      characters: entry.users
    });
    defer.resolve(dbGroup);
  } else {
    defer.reject(new Error('not found'));
  }

  return defer.promise;
};

Group.prototype.getCharacters = function () {
  return this.characters_;
};
