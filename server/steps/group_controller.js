// TODO - probably want to rename  this file

var Q = require('q');
var moment = require('moment');

var logger = require('../logger');
var utils = require('./utils');
var Group = require('./group_model');
var fitbitChartAPI = require('./fitbitChartAPI');

var grouper = module.exports;

//What the client wants
var fake_response = {
  id1: {
    name: 'brent',
    daily: {
      '2015-01-01': {
        steps: 10000,
        processed: {
          points: 8000,
          EODpoints: 8000,
          target: 10000
        }
      },
      '2015-01-02': {
        steps: 10000,
        processed: {
          points: 8000,
          EODpoints: 8000,
          target: 10000
        }
      }
        //...
    }
  },
  id2: {
    // ...
  }
};

/**
 * @typedef UserId {string}
 * @typedef DateId {string}
 * @typedef UserInfo {{name: string, daily: ProcessedDailies}}
 * @typedef ProcessedDailies {Object<DateId, ProcessedStepData}
 * @typedef ProcessedStepData {{steps: number, processed: ProcessedData}}
 * @typedef ProcessedData {{points: number, EODPoints: number, target: number }}
 * @typedef StepData {{steps: number}}
 * @typedef StepDailies {Object<DateId, StepData>}
 */


/**
 * @param {number} groupId
 * @param {string} weekStarting Date of beginning of week in format YYYY-MM-DD
 * @promise {Object<UserId, UserInfo>}
 */
grouper.getData = function (groupId, weekStarting) {

  // find group
  // then get characters
  // then get steps for characters via api
  // then calc points for characters

  var defer = Q.defer();

  Group.find(groupId)
  .then(function (group) {
    return group.getCharacters();
  })
  .then(function (characters) {
    return grouper.getStepsForCharacters_(characters, weekStarting);
  })
  .then(function (stepData) {
    return grouper.calculatePoints_(stepData, weekStarting);
  })
  .then(function (processedDailies) {
    return grouper.addNames_(processedDailies);
  })
  .then(function (stepAndPointData) {
    defer.resolve(stepAndPointData);
  })
  .fail(function (err) {
    logger.error(err);
    defer.reject(err);
  });

  return defer.promise;
};

/**
 * @param {UserId[]} userIds Users we want step data for
 * @param {string} weekStarting Date of beginning of week in format YYYY-MM-DD
 * @promise {Object<UserId, Object<DateId, StepData>>}
 */
grouper.getStepsForCharacters_ = function (userIds, weekStarting) {
  var defer = Q.defer();
  try {
    var perUserData = {};
    var usersLeft = userIds.length;

    // TODO - could pass around moments
    var weekEnding = moment(weekStarting).add(6, 'days').format('YYYY-MM-DD');

    userIds.forEach(function (userId) {
      fitbitChartAPI.getSteps(userId, weekEnding)
      .then(function (perDateData) {
        perUserData[userId] = perDateData;
        usersLeft--;
        if (usersLeft === 0) {
          defer.resolve(perUserData);
        }
      });
    });
  } catch (err) {
    logger.error(err);
    defer.reject(err);
  }

  return defer.promise;
};

/**
 * @param {Object<UserId, Object<DateId, StepData>>} stepData
 * @param {string} weekStarting Date of beginning of week in format YYYY-MM-DD
 * @promise {Object<UserId, ProcessedDailies>
 */
grouper.calculatePoints_ = function (stepData, weekStarting) {
  var defer = Q.defer();
  var pointData = {};
  try {
    var users = Object.keys(stepData);
    users.forEach(function (user) {
      // TODO - actually calculate points
      var userStepData = stepData[user];
      pointData[user] = grouper.calculatePointDataForUser_(userStepData,
        weekStarting);
    });

    defer.resolve(pointData);
  } catch (err) {
    defer.reject(err);
  }
  return defer.promise;
};

/**
 * @param {StepDailies} userStepData
 * @param {string} weekStarting Date of beginning of week in format MM-DD-YYYY
 * @returns {Object<DateId, ProcessedStepData>}
 */
grouper.calculatePointDataForUser_ = function(userStepData, weekStarting) {
  var i, dateKey;
  var userPointData = {};

  var threeWeekTotal = 0;
  var numCountableDays = 0;
  var date = moment.utc(weekStarting);
  for (i = 0; i < 3 * 7; i++) {
    date.subtract(1, 'days');
    dateKey = date.format('YYYY-MM-DD');
    var steps = userStepData[dateKey].steps;
    if (steps > 0) {
      threeWeekTotal += steps;
      numCountableDays++;
    }
  }

  var target = Math.floor(threeWeekTotal / numCountableDays * 0.9);

  // now in PST
  var now = moment.utc().subtract(8, 'hours');

  // TODO - handle future days, EOD points

  // Only return point data for the week
  date = moment.utc(weekStarting);
  for (i = 0; i < 7; i++) {
    dateKey = date.format('YYYY-MM-DD');

    var points = userStepData[dateKey].steps - target;
    var EODPoints = points;
    if (date.isAfter(now)) {
      points = 0;
      EODPoints = 0;
    } else if (date.date() === now.date()) {
      var fraction = now.diff(date) / moment.duration(1, 'days');
      points = Math.floor(userStepData[dateKey].steps - target * fraction);
    }

    userPointData[dateKey] = {
      steps: userStepData[dateKey].steps,
      processed: {
        points: points,
        EODPoints: EODPoints,
        target: target
      }
    };
    date.add(1, 'days');
  }

  return userPointData;
};

// TODO not sure where id -> name belongs properly.
var userToName = {
  '24PBJ8': 'Brent',
  '24PHZH': 'Alyssa',
  '24D7FK': 'Josh',
  '26R3HT': 'Rachel',
  '2TRGWL': 'Liwei',
  '2S3RLC': 'Daniela',
  '24C3MB': 'Zach'
};


/**
 * @param {Object<UserId, ProcessedDailies>
 * @promise {Object<UserId, UserInfo>
 */
grouper.addNames_ = function (processedDailies) {
  // TODO - real clone method
  var obj = {};
  Object.keys(processedDailies).forEach(function (userId) {
    obj[userId] = {
      name: userToName[userId],
      daily: processedDailies[userId]
    };
  });

  return obj;
};
