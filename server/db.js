var pg = require('pg');
var Q = require('q');

var logger = require('./logger');

var URL = process.env.DATABASE_URL;
logger.info('pg URL: ' + URL);

module.exports.performQueryQ = function (str) {
  var defer = Q.defer();

  pg.connect(URL, function (err, client, done) {
    if (err) {
      return defer.reject(err);
    }
    client.query(str, function (err, result) {
      done();

      if (err) {
        console.error('err with query: ' + str);
        console.error(err);
        return defer.reject(err);
      } else {
        return defer.resolve(result);
      }
    });
  });

  return defer.promise;
};

module.exports.performQuery = function (str, callback) {
  pg.connect(URL, function(err, client, done) {
    if (err) {
      console.error('err fetching client from pool: ' + err);
      return callback(err, null);
    }

    client.query(str, function (err, result) {
      done();

      if (err) {
        console.error('err with query: ' + str);
        console.error(err);
        return callback(err, null);
      }

      callback(null, result);

    });
  });
};
