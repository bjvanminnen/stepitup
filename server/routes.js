var express = require('express');
var passport = require('passport');

var fitbit = require('./fitbit');
var authController = require('./authController');

module.exports = function (app) {

  app.use('/', require('./accessLog/accessLog_router'));
  app.use('/chat', require('./chat/chat_router'));
  app.use('/', require('./statics/statics_router'));
  app.use('/steps', require('./steps/steps_router'));

  // statics

  // TODO - move these routes
  app.get('/data/:userId', fitbit.getGraphDataReq);
  app.get('/data/:userId/:date', fitbit.getGraphDataReq);


  app.get('/fitbit/auth', passport.authenticate('fitbit', {session:true}));
  app.get('/fitbit/authCallback',
    passport.authenticate('fitbit', { failureReidrect: '/failure.html' }),
    authController.finishedAuth
  );
  // TODO - provide signout

  // TODO - requireauth for everything else. do it using a route?
  // app.use('/fitbit',
  //   authController.requireAuth
  // );

  app.get('/fitbit/profile',
    authController.requireAuth,
    fitbit.getResource.bind(fitbit, '/profile.json')
  );

  app.get('/fitbit/lastUpdate',
    authController.requireAuth,
    fitbit.getUpdateData
  );
};
