var AccessLog = require('./accessLog_model');
var logger = require('../logger');

var ipLogger = module.exports;

// TODO - should these be in the db? it would make them easier to modify
var ipmap = {
  '24.19.59.25': 'brent-home',
  '50.243.106.93': 'brent-work',
  '73.53.31.241': 'alyssa-home',
  '67.161.114.218': 'josh-home'
};

ipmap.friendlyName = function (ip) {
  return ipmap[ip] || ip;
};


/**
 * Assumes server is on UTC time.
 * // TODO - as a result is broken on localhost
 */
function fakePST(date) {
  if (typeof(date) === 'string') {
    date = new Date(date);
  }
  date.setUTCHours(date.getUTCHours() - 8);
  return date;
}


ipLogger.gets = {};

ipLogger.logAccess = function (req, res, next) {
  if (req.path === '/index.html') {
    var userAgent = req.headers['user-agent'];
    var ip = req.headers['x-forwarded-for'] || req.ip

    var msg = 'IP_user|' + ip + '|' + userAgent;
    logger.info('logAccess - ' + msg);
    var log = new AccessLog({
      message: msg
    });
    log.save(function (err, result) {
      if (err) {
        logger.error('log save err: ' + err);
      }
    });
  }
  next();
}

// TODO - reactive?
var TEMPLATE = "" +
  "<style>" +
  "  table {" +
  "    font-family: verdana,arial,sans-serif;" +
  "    font-size:11px;" +
  "    color:#333333;" +
  "    border-width: 1px;" +
  "    border-color: #666666;" +
  "    border-collapse: collapse;" +
  "  }" +
  "  table th {" +
  "    border-width: 1px;" +
  "    padding: 8px;" +
  "    border-style: solid;" +
  "    border-color: #666666;" +
  "    background-color: #dedede;" +
  "  }" +
  "  table td {" +
  "    border-width: 1px;" +
  "    padding: 8px;" +
  "    border-style: solid;" +
  "    border-color: #666666;" +
  "    background-color: #ffffff;" +
  "  }" +
  "</style>" +
  "<table>" +
  "  <tr>" +
  "    <td>IP</td>" +
  "    <td>fake PST</td>" +
  "    <td>UA</td>" +
  "  </tr>" +
  "  REPLACEME" +
  "</table>";

ipLogger.getAccessInfo = function (req, res) {
  // TODO - move getLogs to model
  AccessLog.getN(50, function (err, result) {
    if (err) {
      res.status(500).send();
    } else {
      var replacement = '';
      result.rows.forEach(function (row) {
        var msg = row.message.split('|');
        var ip = msg[1];
        var userAgent = msg[2];
        replacement += '<tr>';
        replacement += '<td>' + ipmap.friendlyName(ip) + '</td>';
        replacement += '<td>' + fakePST(row.timestamp) + '</td>';
        replacement += '<td>' + userAgent + '</td>';
        replacement += '</tr>';
      });
      res.send(TEMPLATE.replace('REPLACEME', replacement));
    }
  });
}
