var express = require('express');
var router = new express.Router();
module.exports = router;

var AccessLog = require('./accessLog_model');
var ipLogger = require('./ipLogger');

router.get('/index.html', ipLogger.logAccess);
router.get('/accessInfo', ipLogger.getAccessInfo);
