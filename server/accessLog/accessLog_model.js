var db = require('../db');

var TABLE_NAME = 'AccessLogs';

var AccessLog = function(props) {
  this.id = props.id || null;
  this.message = props.message;
  this.timestamp = props.timestamp || null;

  this.persisted_ = false;
};
module.exports = AccessLog;

AccessLog.prototype.save = function (callback) {
  if (process.env.NO_DB) {
    return callback(null);
  }

  var self = this;
  var str;
  callback = callback || function () {};
  if (this.persisted_) {
    str = 'UPDATE ' + TBLE_NAME + ' SET (message, timestamp) = ($$' + this.message +
      '$$, DEFAULT) WHERE id = ' + this.id + 'RETURNING *;';
  } else {
    str = 'INSERT INTO ' + TABLE_NAME + ' (message) VALUES ($$' + this.message + '$$) RETURNING *;';
  }

  db.performQuery(str, function (err, result) {
    if (err) {
      return callback(err, null);
    }
    var row = result.rows[0];
    self.id = row.id;
    self.message = row.message;
    self.timestamp = row.timestamp;
    self.persisted_ = true;
    callback(null, row);

  });

};

AccessLog.getN = function (n, callback) {
  if (process.env.NO_DB) {
    return callback(null, {
      rows: []
    });
  }

  var query = 'SELECT * FROM ' + TABLE_NAME + ' ORDER BY timestamp DESC ' +
    'limit ' + n + ';';

  db.performQuery(query, callback);
};
