var winston = require('winston');
var _ = require('lodash');

var levels = _.clone(winston.config.npm.levels);
levels.db = 8;

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      colorize: true
    }),
  ],
  levels: levels
});

// logger.setLevels(levels);
winston.addColors({db: 'cyan'});

module.exports = logger;

logger.hide = function () {
  logger.transports = [];
};

var logger_error = logger.error;
logger.error = function (val) {
  if (val.stack) {
    val = val.stack;
  }
  logger_error(val);
};
