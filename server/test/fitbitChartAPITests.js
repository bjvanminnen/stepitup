var assert = require("assert")

var fitbitChartAPI = require('../steps/fitbitChartAPI');

var testUtils = require('./testUtils');

assert.equal(process.env.NODE_ENV, 'test');

describe('getSteps', function () {
  before(function () {
    testUtils.nockFitbitChartAPI();
  });

  after(function () {
    testUtils.clearNock();
  });

  it('with date specified', function (done) {
    fitbitChartAPI.getSteps(testUtils.users.brent.id, '2015-02-10')
    .then(function (result) {
      assert(result);
      var dateKeys = Object.keys(result);
      assert.equal(dateKeys.length, 31);
      assert.equal(result['2015-02-10'].steps, 10375);
      assert.equal(result['2015-01-11'].steps, 10278);
      dateKeys.forEach(function (dateKey) {
        assert(result[dateKey].steps);
      });
      done();
    }).fail(done);
  });
});


// TODO - test for dateFromDailyInfo_/stepsFromDailyInfo_?
