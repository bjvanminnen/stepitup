var assert = require("assert")
var Q = require('q');
var request = require('supertest');

var app = require('../app');
var db = require('../db');
var User = require('../models/User');

var testUser = {
  id: '36KHP9',
  token: '20399362b445329b5c9b2d15b946f67e',
  tokenSecret: 'f1e440c35ffd43e82fdf4b9e616dfba9',
};

function emptyUserTable() {
  return db.performQueryQ('DELETE FROM Users;')
}

describe('user', function () {

  it('can save a new user to the db', function (done) {
    var user = new User(testUser);
    emptyUserTable()
    .then(function () {
      return user.save();
    })
    .then(function (result) {
      assert(result instanceof User);
      assert.equal(result.id, testUser.id);
      assert.equal(result.token, testUser.token);
      assert.equal(result.tokenSecret, testUser.tokenSecret);
      assert(result.lastModified);
      assert(result.persisted_);
      assert.equal(result.persisted_.id, testUser.id);
      assert.equal(result.persisted_.token, testUser.token);
      assert.equal(result.persisted_.tokenSecret, testUser.tokenSecret);
      var expected = JSON.parse(JSON.stringify(result.lastModified));
      assert.equal(result.persisted_.lastModified, expected);
      done();
    })
    .fail(function (err) {
      done(err);
    });
  });

  it('can find user in db', function (done) {
    var user = new User(testUser);
    emptyUserTable()
    .then(function () {
      return user.save();
    })
    .then(function (savedUser) {
      return User.find(testUser.id);
    })
    .then(function (dbUser) {
      assert.equal(dbUser.id, testUser.id);
      assert.equal(dbUser.token, testUser.token);
      assert.equal(dbUser.tokenSecret, testUser.tokenSecret);
      done();
    })
    .catch(function (err) {
      done(err);
    });

  });

  it('doenst hit db when saving user with no changes', function (done) {
    done();

  });

});
