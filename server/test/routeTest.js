var assert = require("assert")
var request = require('supertest');

var app = require('../app');

assert.equal(process.env.NODE_ENV, 'test');

describe('static routes', function () {
  it('/index.html', function (done) {
    request(app)
    .get('/index.html')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      done();
    });
  });

  it('/', function (done) {
    request(app)
    .get('/')
    .expect(302)
    .expect('location', '/index.html')
    .end(function (err, response) {
      assert(!err, err);
      done();
    });
  });

  it('/failure.html', function (done) {
    request(app)
    .get('/failure.html')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      assert(/Auth failure/.test(response.text));
      done();
    });
  });

  it('/source.js', function (done) {
    request(app)
    .get('/source.js')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      done();
    });
  });

  it('/style', function (done) {
    request(app)
    .get('/style/style.css')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      done();
    });
  });
});

describe('accessInfo', function () {
  it('logs to db when i go to index.html', function () {
    // TODO
  });

  it('/accessInfo', function (done) {
    // TODO - could also make sure this gets updated

    request(app)
    .get('/accessInfo')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      done();
    });
  });


});
