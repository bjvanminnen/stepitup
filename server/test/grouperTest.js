var assert = require("assert")

var grouper = require('../steps/group_controller');

var testUtils = require('./testUtils');

assert.equal(process.env.NODE_ENV, 'test');

describe('getData', function () {
  before(function () {
    testUtils.nockFitbitChartAPI();
  });

  after(function () {
    testUtils.clearNock();
  });

  it('works for group 2', function (done) {
    var groupId = 2;
    grouper.getData(groupId, '2015-02-04')
    .then(function (results) {
      var userKeys = Object.keys(results);
      assert.equal(userKeys.length, 2);
      userKeys.forEach(function (userKey) {
        var userData = results[userKey];
        assert(userData.name);
        assert(userData.daily);
        var dateKeys = Object.keys(userData.daily);
        dateKeys.forEach(function (dateKey) {
          var stepData = userData.daily[dateKey];
          assert(typeof(stepData.steps) === 'number');
          assert(stepData.processed);
          assert(typeof(stepData.processed.points) === 'number');
          assert(typeof(stepData.processed.EODPoints) === 'number');
          assert(typeof(stepData.processed.target) === 'number');
        })
      })
    })
    .then(function () { done(); })
    .fail(done);
  });
});

describe('getStepsForCharacters_', function () {
  before(function () {
    testUtils.nockFitbitChartAPI();
  });

  after(function () {
    testUtils.clearNock();
  });

  it('works with two characters', function (done) {
    var users = [
      testUtils.users.brent.id,
      testUtils.users.alyssa.id
    ];

    grouper.getStepsForCharacters_(users, '2015-02-04')
    .then(function (result) {
      var userKeys = Object.keys(result);
      assert.deepEqual(userKeys.sort(), users.sort());
      userKeys.forEach(function (userKey) {
        var userData = result[userKey];
        var dateKeys = Object.keys(userData);
        assert.equal(dateKeys.length, 31);
        dateKeys.forEach(function (dateKey) {
          var stepData = userData[dateKey];
          assert(typeof(stepData.steps) === 'number');
        });
      });
    })
    .then(function () { done(); })
    .fail(done);
  });
});

describe('calculatePoints_', function () {
  before(function () {
    testUtils.nockFitbitChartAPI();
  });

  after(function () {
    testUtils.clearNock();
  });

  var inputData = {
    'firstId': {
      '2014-01-01': { steps: 10000 },
      '2014-01-02': { steps: 10000 },
      '2014-01-03': { steps: 10000 },
      '2014-01-04': { steps: 10000 },
      '2014-01-05': { steps: 10000 },
      '2014-01-06': { steps: 10000 },
      '2014-01-07': { steps: 10000 },
      '2014-01-08': { steps: 10000 },
      '2014-01-09': { steps: 10000 },
      '2014-01-10': { steps: 10000 },
      '2014-01-11': { steps: 10000 },
      '2014-01-12': { steps: 10000 },
      '2014-01-13': { steps: 10000 },
      '2014-01-14': { steps: 10000 },
      '2014-01-15': { steps: 10000 },
      '2014-01-16': { steps: 10000 },
      '2014-01-17': { steps: 10000 },
      '2014-01-18': { steps: 10000 },
      '2014-01-19': { steps: 10000 },
      '2014-01-20': { steps: 10000 },
      '2014-01-21': { steps: 10000 },
      '2014-01-22': { steps: 10000 },
      '2014-01-23': { steps: 10000 },
      '2014-01-24': { steps: 10000 },
      '2014-01-25': { steps: 10000 },
      '2014-01-26': { steps: 10000 },
      '2014-01-27': { steps: 10000 },
      '2014-01-28': { steps: 10000 },
      '2014-01-29': { steps: 10000 },
      '2014-01-30': { steps: 10000 },
      '2014-01-31': { steps: 10000 }
    },
    'secondId': {
      '2014-01-01': { steps: 10000 },
      '2014-01-02': { steps: 10000 },
      '2014-01-03': { steps: 10000 },
      '2014-01-04': { steps: 10000 },
      '2014-01-05': { steps: 10000 },
      '2014-01-06': { steps: 10000 },
      '2014-01-07': { steps: 10000 },
      '2014-01-08': { steps: 10000 },
      '2014-01-09': { steps: 10000 },
      '2014-01-10': { steps: 10000 },
      '2014-01-11': { steps: 10000 },
      '2014-01-12': { steps: 10000 },
      '2014-01-13': { steps: 10000 },
      '2014-01-14': { steps: 10000 },
      '2014-01-15': { steps: 10000 },
      '2014-01-16': { steps: 10000 },
      '2014-01-17': { steps: 10000 },
      '2014-01-18': { steps: 10000 },
      '2014-01-19': { steps: 10000 },
      '2014-01-20': { steps: 10000 },
      '2014-01-21': { steps: 10000 },
      '2014-01-22': { steps: 10000 },
      '2014-01-23': { steps: 10000 },
      '2014-01-24': { steps: 10000 },
      '2014-01-25': { steps: 10000 },
      '2014-01-26': { steps: 10000 },
      '2014-01-27': { steps: 10000 },
      '2014-01-28': { steps: 10000 },
      '2014-01-29': { steps: 10000 },
      '2014-01-30': { steps: 10000 },
      '2014-01-31': { steps: 10000 }
    }
  };

  it('behaves as expected', function (done) {
    // TODO - ultimately calculating points/target is going to involve looking
    // at historical data. that means either (a) querying more data or
    // (b) throwing some of this data out
    grouper.calculatePoints_(inputData, '2014-01-25')
    .then(function (pointData) {
      var userIds = Object.keys(pointData);
      assert.equal(userIds.length, Object.keys(inputData).length);
      userIds.forEach(function (userId) {
        var userData = pointData[userId];
        assert.equal(Object.keys(userData).length, 7);
        var dates = Object.keys(userData);
        dates.forEach(function (dateId) {
          assert(userData[dateId].processed);
          assert(userData[dateId].processed.points);
          assert(userData[dateId].processed.EODPoints);
          assert.equal(userData[dateId].processed.target, 9000);
        });
      });
    })
    .then(function () { done(); })
    .fail(done);
  });

  it('works using getStepsForCharacters_ as input', function (done) {
    var users = [
      testUtils.users.brent.id,
      testUtils.users.alyssa.id
    ];

    grouper.getStepsForCharacters_(users, '2015-02-04')
    .then(function (stepData) {
      return grouper.calculatePoints_(stepData, '2015-02-04');
    })
    .then(function (pointData) {
      assert(pointData);
      var userKeys = Object.keys(pointData);
      assert.deepEqual(userKeys.sort(), users.sort());

      assert.equal(
        pointData[testUtils.users.brent.id]['2015-02-04'].steps,
        11150
      );
      assert.equal(
        pointData[testUtils.users.brent.id]['2015-02-04'].processed.points,
        2825
      );
      assert.equal(
        pointData[testUtils.users.brent.id]['2015-02-04'].processed.EODPoints,
        2825
      );
      assert.equal(
        pointData[testUtils.users.brent.id]['2015-02-04'].processed.target,
        8325
      );

    })
    .then(function () { done(); })
    .fail(done);
  });

  // TODO - proper test for EODPoints
});

describe('addNames_', function () {
  it('works using calculatePoints_ as input', function (done) {
    done();
  });
});
