var assert = require("assert")
var request = require('supertest');

var app = require('../app');

var testUtils = require('./testUtils');

// Use my ID as test user, since actual test user has some strange behavior
var testUser = {
  id: '24PBJ8'
};

assert.equal(process.env.NODE_ENV, 'test');

describe('steps routes', function () {
  before(function () {
    testUtils.nockFitbitChartAPI();
  });

  after(function () {
    testUtils.clearNock();
  });

  it('can get group step data', function (done) {
    request(app)
    .get('/steps/group/2/2015-02-04')
    .expect(200)
    .end(function (err, response) {
      assert(!err, err);
      var obj = JSON.parse(response.text);
      var userIds = Object.keys(obj);
      assert.equal(userIds.length, 2);
      done();
    });
  });

  it('fails when using non-existing group', function (done) {
    request(app)
    .get('/steps/group/nonexistent/2015-02-04')
    .expect(500)
    .end(function (err, response) {
      if (err) {
        return done(err);
      }
      done();
    });
  });
});
