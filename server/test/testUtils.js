var http = require('http');
var nock = require('nock');
// var EventEmitter = require('events').EventEmitter;

var testUtils = module.exports;

testUtils.users = {
  upwardMobility: {
    id: '36KHP9',
    token: '20399362b445329b5c9b2d15b946f67e',
    tokenSecret: 'f1e440c35ffd43e82fdf4b9e616dfba9',
    email: 'bjvanminnen+upwardmobility@gmail.com'
  },
  brent: {
    id: '24PBJ8'
  },
  alyssa: {
    id: '24PHZH'
  }
};

// TODO - should i have ability to run unnocked on occasion?

testUtils.nockFitbitChartAPI = function () {
  nock('http://www.fitbit.com')
  .get('/graph/getGraphData?userId=' + testUtils.users.brent.id +
    '&type=stepsTaken&period=1m&apiFormat=json&dateTo=2015-02-10')
  .reply(200, testUtils.responses.getGraphDataBrent02102015);

  nock('http://www.fitbit.com')
  .get('/graph/getGraphData?userId=' + testUtils.users.alyssa.id +
    '&type=stepsTaken&period=1m&apiFormat=json&dateTo=2015-02-10')
  .reply(200, testUtils.responses.getGraphDataAlyssa02102015);
};

testUtils.clearNock = function () {
  nock.cleanAll();
};

testUtils.responses = {
  // http://www.fitbit.com/graph/getGraphData?userId=24PBJ8&type=stepsTaken&period=1m&apiFormat=json&dateTo=2015-02-10
  getGraphDataBrent02102015: {
    "result":{
      "chart":{
        "columnWidth":70,
        "datasets":[
          [
            {
              "description":"10278 steps taken on Sun, Jan 11",
              "label":"Jan 11",
              "name":"1420934400000",
              "url":"/activities/2015/01/11",
              "value":10278
            },
            {
              "description":"9091 steps taken on Mon, Jan 12",
              "label":"Jan 12",
              "name":"1421020800000",
              "url":"/activities/2015/01/12",
              "value":9091
            },
            {
              "description":"11192 steps taken on Tue, Jan 13",
              "label":"Jan 13",
              "name":"1421107200000",
              "url":"/activities/2015/01/13",
              "value":11192
            },
            {
              "description":"12620 steps taken on Wed, Jan 14",
              "label":"Jan 14",
              "name":"1421193600000",
              "url":"/activities/2015/01/14",
              "value":12620
            },
            {
              "description":"10445 steps taken on Thu, Jan 15",
              "label":"Jan 15",
              "name":"1421280000000",
              "url":"/activities/2015/01/15",
              "value":10445
            },
            {
              "description":"11945 steps taken on Fri, Jan 16",
              "label":"Jan 16",
              "name":"1421366400000",
              "url":"/activities/2015/01/16",
              "value":11945
            },
            {
              "description":"4630 steps taken on Sat, Jan 17",
              "label":"Jan 17",
              "name":"1421452800000",
              "url":"/activities/2015/01/17",
              "value":4630
            },
            {
              "description":"4834 steps taken on Sun, Jan 18",
              "label":"Jan 18",
              "name":"1421539200000",
              "url":"/activities/2015/01/18",
              "value":4834
            },
            {
              "description":"10712 steps taken on Mon, Jan 19",
              "label":"Jan 19",
              "name":"1421625600000",
              "url":"/activities/2015/01/19",
              "value":10712
            },
            {
              "description":"6896 steps taken on Tue, Jan 20",
              "label":"Jan 20",
              "name":"1421712000000",
              "url":"/activities/2015/01/20",
              "value":6896
            },
            {
              "description":"10473 steps taken on Wed, Jan 21",
              "label":"Jan 21",
              "name":"1421798400000",
              "url":"/activities/2015/01/21",
              "value":10473
            },
            {
              "description":"11678 steps taken on Thu, Jan 22",
              "label":"Jan 22",
              "name":"1421884800000",
              "url":"/activities/2015/01/22",
              "value":11678
            },
            {
              "description":"8069 steps taken on Fri, Jan 23",
              "label":"Jan 23",
              "name":"1421971200000",
              "url":"/activities/2015/01/23",
              "value":8069
            },
            {
              "description":"6919 steps taken on Sat, Jan 24",
              "label":"Jan 24",
              "name":"1422057600000",
              "url":"/activities/2015/01/24",
              "value":6919
            },
            {
              "description":"6707 steps taken on Sun, Jan 25",
              "label":"Jan 25",
              "name":"1422144000000",
              "url":"/activities/2015/01/25",
              "value":6707
            },
            {
              "description":"9997 steps taken on Mon, Jan 26",
              "label":"Jan 26",
              "name":"1422230400000",
              "url":"/activities/2015/01/26",
              "value":9997
            },
            {
              "description":"8265 steps taken on Tue, Jan 27",
              "label":"Jan 27",
              "name":"1422316800000",
              "url":"/activities/2015/01/27",
              "value":8265
            },
            {
              "description":"11780 steps taken on Wed, Jan 28",
              "label":"Jan 28",
              "name":"1422403200000",
              "url":"/activities/2015/01/28",
              "value":11780
            },
            {
              "description":"10105 steps taken on Thu, Jan 29",
              "label":"Jan 29",
              "name":"1422489600000",
              "url":"/activities/2015/01/29",
              "value":10105
            },
            {
              "description":"9078 steps taken on Fri, Jan 30",
              "label":"Jan 30",
              "name":"1422576000000",
              "url":"/activities/2015/01/30",
              "value":9078
            },
            {
              "description":"5229 steps taken on Sat, Jan 31",
              "label":"Jan 31",
              "name":"1422662400000",
              "url":"/activities/2015/01/31",
              "value":5229
            },
            {
              "description":"13582 steps taken on Sun, Feb 1",
              "label":"Feb 01",
              "name":"1422748800000",
              "url":"/activities/2015/02/01",
              "value":13582
            },
            {
              "description":"9034 steps taken on Mon, Feb 2",
              "label":"Feb 02",
              "name":"1422835200000",
              "url":"/activities/2015/02/02",
              "value":9034
            },
            {
              "description":"11255 steps taken on Tue, Feb 3",
              "label":"Feb 03",
              "name":"1422921600000",
              "url":"/activities/2015/02/03",
              "value":11255
            },
            {
              "description":"11150 steps taken on Wed, Feb 4",
              "label":"Feb 04",
              "name":"1423008000000",
              "url":"/activities/2015/02/04",
              "value":11150
            },
            {
              "description":"8376 steps taken on Thu, Feb 5",
              "label":"Feb 05",
              "name":"1423094400000",
              "url":"/activities/2015/02/05",
              "value":8376
            },
            {
              "description":"7747 steps taken on Fri, Feb 6",
              "label":"Feb 06",
              "name":"1423180800000",
              "url":"/activities/2015/02/06",
              "value":7747
            },
            {
              "description":"2619 steps taken on Sat, Feb 7",
              "label":"Feb 07",
              "name":"1423267200000",
              "url":"/activities/2015/02/07",
              "value":2619
            },
            {
              "description":"14579 steps taken on Sun, Feb 8",
              "label":"Feb 08",
              "name":"1423353600000",
              "url":"/activities/2015/02/08",
              "value":14579
            },
            {
              "description":"9554 steps taken on Mon, Feb 9",
              "label":"Feb 09",
              "name":"1423440000000",
              "url":"/activities/2015/02/09",
              "value":9554
            },
            {
              "description":"10375 steps taken on Tue, Feb 10",
              "label":"Feb 10",
              "name":"1423526400000",
              "url":"/activities/2015/02/10",
              "value":10375
            }
          ]
        ],
        "expiresOn":1424073600000,
        "gridCategoryAlpha":0,
        "gridValueAlpha":100,
        "isCanvasBorderOn":true,
        "isDescEnabled":true,
        "isLinkEnabled":true,
        "isSkipZeros":false,
        "maxAge":10358,
        "plotAreaAlpha":100,
        "plotAreaBorderAlpha":0,
        "plotAreaColor":"#ffffff",
        "plotAreaMarginsBottom":40,
        "plotAreaMarginsLeft":40,
        "plotAreaMarginsRight":20,
        "plotAreaMarginsTop":5,
        "precision":0,
        "rotateNames":0,
        "timePeriod":"ONE_MONTH",
        "valuesCategoryEnabled":true,
        "valuesCategoryFrequency":6,
        "valuesValueEnabled":true,
        "valuesValueFrequency":1,
        "valuesValueIntegersOnly":false,
        "yMax":14579,
        "yMin":0
      },
      "success":true
    }
  },
  getGraphDataAlyssa02102015: {
    "result":{
      "chart":{
        "columnWidth":70,
        "datasets":[
          [
            {
              "description":"12860 steps taken on Sun, Jan 11",
              "label":"Jan 11",
              "name":"1420934400000",
              "url":"/activities/2015/01/11",
              "value":12860
            },
            {
              "description":"13937 steps taken on Mon, Jan 12",
              "label":"Jan 12",
              "name":"1421020800000",
              "url":"/activities/2015/01/12",
              "value":13937
            },
            {
              "description":"13235 steps taken on Tue, Jan 13",
              "label":"Jan 13",
              "name":"1421107200000",
              "url":"/activities/2015/01/13",
              "value":13235
            },
            {
              "description":"17574 steps taken on Wed, Jan 14",
              "label":"Jan 14",
              "name":"1421193600000",
              "url":"/activities/2015/01/14",
              "value":17574
            },
            {
              "description":"20434 steps taken on Thu, Jan 15",
              "label":"Jan 15",
              "name":"1421280000000",
              "url":"/activities/2015/01/15",
              "value":20434
            },
            {
              "description":"17248 steps taken on Fri, Jan 16",
              "label":"Jan 16",
              "name":"1421366400000",
              "url":"/activities/2015/01/16",
              "value":17248
            },
            {
              "description":"8014 steps taken on Sat, Jan 17",
              "label":"Jan 17",
              "name":"1421452800000",
              "url":"/activities/2015/01/17",
              "value":8014
            },
            {
              "description":"4630 steps taken on Sun, Jan 18",
              "label":"Jan 18",
              "name":"1421539200000",
              "url":"/activities/2015/01/18",
              "value":4630
            },
            {
              "description":"21700 steps taken on Mon, Jan 19",
              "label":"Jan 19",
              "name":"1421625600000",
              "url":"/activities/2015/01/19",
              "value":21700
            },
            {
              "description":"14379 steps taken on Tue, Jan 20",
              "label":"Jan 20",
              "name":"1421712000000",
              "url":"/activities/2015/01/20",
              "value":14379
            },
            {
              "description":"18571 steps taken on Wed, Jan 21",
              "label":"Jan 21",
              "name":"1421798400000",
              "url":"/activities/2015/01/21",
              "value":18571
            },
            {
              "description":"15106 steps taken on Thu, Jan 22",
              "label":"Jan 22",
              "name":"1421884800000",
              "url":"/activities/2015/01/22",
              "value":15106
            },
            {
              "description":"13741 steps taken on Fri, Jan 23",
              "label":"Jan 23",
              "name":"1421971200000",
              "url":"/activities/2015/01/23",
              "value":13741
            },
            {
              "description":"14868 steps taken on Sat, Jan 24",
              "label":"Jan 24",
              "name":"1422057600000",
              "url":"/activities/2015/01/24",
              "value":14868
            },
            {
              "description":"11892 steps taken on Sun, Jan 25",
              "label":"Jan 25",
              "name":"1422144000000",
              "url":"/activities/2015/01/25",
              "value":11892
            },
            {
              "description":"17389 steps taken on Mon, Jan 26",
              "label":"Jan 26",
              "name":"1422230400000",
              "url":"/activities/2015/01/26",
              "value":17389
            },
            {
              "description":"12460 steps taken on Tue, Jan 27",
              "label":"Jan 27",
              "name":"1422316800000",
              "url":"/activities/2015/01/27",
              "value":12460
            },
            {
              "description":"11417 steps taken on Wed, Jan 28",
              "label":"Jan 28",
              "name":"1422403200000",
              "url":"/activities/2015/01/28",
              "value":11417
            },
            {
              "description":"0 steps taken on Thu, Jan 29",
              "label":"Jan 29",
              "name":"1422489600000",
              "url":"/activities/2015/01/29",
              "value":0
            },
            {
              "description":"21871 steps taken on Fri, Jan 30",
              "label":"Jan 30",
              "name":"1422576000000",
              "url":"/activities/2015/01/30",
              "value":21871
            },
            {
              "description":"12736 steps taken on Sat, Jan 31",
              "label":"Jan 31",
              "name":"1422662400000",
              "url":"/activities/2015/01/31",
              "value":12736
            },
            {
              "description":"21465 steps taken on Sun, Feb 1",
              "label":"Feb 01",
              "name":"1422748800000",
              "url":"/activities/2015/02/01",
              "value":21465
            },
            {
              "description":"11046 steps taken on Mon, Feb 2",
              "label":"Feb 02",
              "name":"1422835200000",
              "url":"/activities/2015/02/02",
              "value":11046
            },
            {
              "description":"12867 steps taken on Tue, Feb 3",
              "label":"Feb 03",
              "name":"1422921600000",
              "url":"/activities/2015/02/03",
              "value":12867
            },
            {
              "description":"4455 steps taken on Wed, Feb 4",
              "label":"Feb 04",
              "name":"1423008000000",
              "url":"/activities/2015/02/04",
              "value":4455
            },
            {
              "description":"14095 steps taken on Thu, Feb 5",
              "label":"Feb 05",
              "name":"1423094400000",
              "url":"/activities/2015/02/05",
              "value":14095
            },
            {
              "description":"9989 steps taken on Fri, Feb 6",
              "label":"Feb 06",
              "name":"1423180800000",
              "url":"/activities/2015/02/06",
              "value":9989
            },
            {
              "description":"7477 steps taken on Sat, Feb 7",
              "label":"Feb 07",
              "name":"1423267200000",
              "url":"/activities/2015/02/07",
              "value":7477
            },
            {
              "description":"5928 steps taken on Sun, Feb 8",
              "label":"Feb 08",
              "name":"1423353600000",
              "url":"/activities/2015/02/08",
              "value":5928
            },
            {
              "description":"10316 steps taken on Mon, Feb 9",
              "label":"Feb 09",
              "name":"1423440000000",
              "url":"/activities/2015/02/09",
              "value":10316
            },
            {
              "description":"18918 steps taken on Tue, Feb 10",
              "label":"Feb 10",
              "name":"1423526400000",
              "url":"/activities/2015/02/10",
              "value":18918
            }
          ]
        ],
        "expiresOn":1424160000000,
        "gridCategoryAlpha":0,
        "gridValueAlpha":100,
        "isCanvasBorderOn":true,
        "isDescEnabled":true,
        "isLinkEnabled":true,
        "isSkipZeros":false,
        "maxAge":937,
        "plotAreaAlpha":100,
        "plotAreaBorderAlpha":0,
        "plotAreaColor":"#ffffff",
        "plotAreaMarginsBottom":40,
        "plotAreaMarginsLeft":40,
        "plotAreaMarginsRight":20,
        "plotAreaMarginsTop":5,
        "precision":0,
        "rotateNames":0,
        "timePeriod":"ONE_MONTH",
        "valuesCategoryEnabled":true,
        "valuesCategoryFrequency":6,
        "valuesValueEnabled":true,
        "valuesValueFrequency":1,
        "valuesValueIntegersOnly":false,
        "yMax":21871,
        "yMin":0
      },
      "success":true
    }
  }

};
