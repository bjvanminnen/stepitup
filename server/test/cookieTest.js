var express = require('express');
var request = require('supertest');
var assert = require('assert');
var cookieParser = require('cookie-parser');

var accessInfo = require('cookiejar').CookieAccessInfo("", "/");
request.agent.prototype.getCookie = function (name) {
  return this.jar.getCookie(name, accessInfo);
};

request.agent.prototype.getCookies = function () {
  return this.jar.getCookies(accessInfo);
};

var app = express();
app.use(cookieParser());

app.get('/givemecookie', function (req, res) {
  res.cookie('FOO', 'bar', { maxAge: 900000 });
  res.send('cookied');
});

describe('agent cookie', function () {
  var agent = request.agent(app);
  it('sets cookie', function (done) {
    agent.get('/givemecookie')
      .expect('set-cookie', /FOO=bar/)
      .end(function (err, result) {
        assert(agent);
        assert(!err, err);
        // var cookies = agent.jar.getCookies(CookieAccessInfo("", "/"));
        assert(agent.getCookie('FOO'));
        assert(agent.getCookies().length === 1);
        done();
      });
  });

  // it('sends cookie', function (done) {
  //   agent.get('/givemecookie')
  //     .expect('FOO', done);
  // });
});
