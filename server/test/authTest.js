var assert = require("assert")
var request = require('supertest');
var stSession = require('supertest-session');

var app = require('../app');
var db = require('../db');
var testUser = require('./testUtils').users.upwardMobility;
var Session = stSession({app: app});

var User = require('../models/User');

var ONE_YEAR = 365 * 24 * 60 * 60 * 1000;

// simulate getting a cookie from authenticating successfully
app.get('/getTestUserCookie', function (req, res) {
  res.cookie('userId', testUser.id, { maxAge: ONE_YEAR, signed: true });
  res.status(200).send();
});

describe('authenticate from scratch', function () {
  var sess;

  before(function (done) {
    sess = new Session();
    assert(process.env.FITBIT_CONSUMER_KEY, 'Has secrets');
    assert(process.env.FITBIT_CONSUMER_SECRET);

    // Remove all uses
    db.performQuery('DELETE FROM Users;', function (err, results) {
      assert(!err, err);
      done();
    });
  });

  after(function () {
    sess.destroy();
  });

  it('has no userId when hitting index', function (done) {
    sess.get('/index.html').end(function (err, result) {
      assert(!err, err);
      assert.equal(sess.cookies.length, 1);
      assert(sess.cookies[0]['connect.sid']);
      done();
    });
  });

  it('cant hit /fitbit/profile', function (done) {
    sess.get('/fitbit/profile')
      .expect(401)
      .end(function (err, response) {
        done(err);
      });
  });

  var fitbitLocation;
  var oauth_token;

  it('gets redirected from fitbit/auth', function (done) {
    this.timeout(6000);
    sess.get('/fitbit/auth')
      .expect(302)
      .end(function (err, response) {
        assert(!err, err);
        fitbitLocation = response.headers['location'];
        var re = /https:\/\/www.fitbit.com\/oauth\/authorize\?oauth_token=(.*)$/;
        var match = re.exec(fitbitLocation);
        assert(match);
        oauth_token = match[1];
        assert(oauth_token);
        done();
      });
  });

  // it('auths with fitbit', function (done) {
  //   var wrap = require('../wrapHttp');
  //   wrap.config.showResponse = false;

  //   request('https://www.fitbit.com')
  //     .post('/oauth/oauth_login_allow')
  //     .set('Referrer', 'https://www.fitbit.com/oauth/authorize?oauth_token=ddfdc3648b4b4faa75d3a9802a18a6c4')
  //     .send('oauth_token', oauth_token)
  //     .send('email', 'bjvanminnen@gmail.com')
  //     .send('password', 'bublegum') // TODO - dont commit secret
  //     .expect(200)
  //     .end(function (err, response) {
  //       assert(!err, err);
  //       wrap.detach();
  //       done();
  //     });
  // });

  it('add fake user', function (done) {
    var dbUser = new User(testUser);
    dbUser.save()
    .then(function () {
      done();
    })
    .fail(function (err) {
      done(err);
    });
  });

  it('get cookie for fake user', function (done) {
    sess.get('/getTestUserCookie')
      .expect(200)
      .end(function (err, response) {
        assert(!err, err);
        var userIdCookie;
        assert(sess.cookies.some(function (cookie) {
          return !!cookie.userId;
        }));
        done();
      });
  });

  it('can now hit /fitbit/profile', function (done) {
    this.timeout(8000);
    sess.get('/fitbit/profile')
      .expect(200)
      .end(function (err, response) {
        done(err);
      });
  });


 });
