var passport = require('passport');
var OAuthStrategy = require('passport-oauth').OAuthStrategy;
var Q = require('q');

var User = require('./models/User');
var logger = require('./logger');

var ONE_YEAR = 365 * 24 * 60 * 60 * 1000;

// TODO - support auth-less mode? (i.e. no secret set)
module.exports.fitbitStrategy = new OAuthStrategy({
  requestTokenURL: 'https://api.fitbit.com/oauth/request_token',
  accessTokenURL: 'https://api.fitbit.com/oauth/access_token',
  userAuthorizationURL: 'https://www.fitbit.com/oauth/authorize',
  consumerKey: process.env.FITBIT_CONSUMER_KEY || 'none',
  consumerSecret: process.env.FITBIT_CONSUMER_SECRET || 'none',
  callbackURL: '/fitbit/authCallback'
}, function (token, tokenSecret, params, profile, done) {
  var id = params.encoded_user_id;
  done(null, {
    id: id,
    token: token,
    tokenSecret: tokenSecret
  });
});

module.exports.authenticateFromCookie = function authenticateFromCookie (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  var userId = req.signedCookies.userId;
  if (!userId) {
    return next();
  }
  ('authenticateFrom: ' + userId);
  User.find(userId)
  .then(function (dbUser) {
    // TODO - test this pattern
    logger.info('login');
    return Q.ninvoke(req, 'login', dbUser, {session: true});
  })
  .fail(function (err) {
    logger.info('clear cookie');
    res.clearCookie('userId');
  })
  .then(function () {
    next();
  });
};

/**
 * Middleware that fails if we're not auth'd
 */
module.exports.requireAuth = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.status(401).send();
};

module.exports.finishedAuth = function (req, res, next) {
  logger.info('set cookie');
  res.cookie('userId', req.user.id, { maxAge: ONE_YEAR, signed: true });
  res.redirect('/index.html');
};

/**
 * serialize the user to the db, either replacing existing user if we have one
 * of this id, or creating a new one.
 */
passport.serializeUser(function (user, done) {
  User.find(user.id)
  .then(function (dbUser) {
    return dbUser;
  })
  .fail(function (err) {
    return new User(user);
  })
  .then(function (dbUser) {
    // update user
    dbUser.token = user.token;
    dbUser.tokenSecret = user.tokenSecret;
    return dbUser.save();
  })
  .then(function (savedUser) {
    done(null, user.id);
  })
  .catch(function (err) {
    logger.error(err);
    done(err, null);
  });
});

passport.deserializeUser(function (id, done) {
  User.find(id)
  .then(function (dbUser) {
    done(null, dbUser);
  })
  .fail(function (err) {
    done(err, null);
  });
});
