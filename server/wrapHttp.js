var config = {
  showResponse: true,
  attached: true
};
module.exports.config = config;

var protocols = ['http', 'https'];

function prettyJSON(obj) {
  console.log(JSON.stringify(obj, null, 2));
}

protocols.forEach(function (protocolString) {
  var protocol = require(protocolString);

  var orig = protocol.request;
  protocol.request = function (options, cb) {
    var request = orig(options, cb);
    if (!config.attached) {
      return request;
    }

    console.log('<Request - ' + protocolString + '>');
    prettyJSON(options);
    console.log('</Request>');


    request.on('response', function (response) {
      console.log('<Response>');
      console.log('<headers>');
      prettyJSON(response.headers);
      console.log('</headers>');

      response.on('data', function (chunk) {
        if (config.showResponse) {
          console.log(chunk.toString());
        }
      });
      response.on('end', function () {
        console.log('</Response>');
      });
    });

    return request;
  };
});

module.exports.detach = function () {
  config.attach = false;
};
