var http = require('http');
var FitbitClient = require('fitbit-node');

var logger = require('./logger');

if (!process.env.FITBIT_CONSUMER_KEY || !process.env.FITBIT_CONSUMER_SECRET) {
  logger.warn('ALERT: No consumer key/secret. Fitbit auth will fail');
}

// TODO - okay to share this?
var client = new FitbitClient(
  process.env.FITBIT_CONSUMER_KEY || 'none',
  process.env.FITBIT_CONSUMER_SECRET || 'none'
);

// TODO - collapse these
module.exports.getGraphDataReq = function (req, res, next) {
  module.exports.getGraphData(req, res, function (err, result) {
    if (err) {
      logger.error('getGraphData err: ' + err);
      res.status(500).send({ error: err });
    } else {
      res.send(result);
    }
  });
};

module.exports.getGraphData = function (req, res, callback) {
  var userId = req.params.userId;
  var date = req.params.date;

  //&dateTo=2014-11-23
  var url = 'http://www.fitbit.com/graph/getGraphData?userId=' + userId + '&type=stepsTaken&period=1m&apiFormat=json';
  if (date) {
    url += "&dateTo=" + date;
  }
  http.get(url, function (res) {
    var body = '';
    res.on('data', function (chunk) {
      body += chunk;
    });
    res.on('end', function () {
      callback(null, body);
    });
  }).on('error', function (e) {
    logger.error("error: " + e.message);
    callback(e, null);
  });
};

module.exports.getResource = function (resource, req, res, next) {
  client.requestResource(resource, 'GET', req.user.token,
    req.user.tokenSecret, req.user.id)
  .then(function (results) {
    var data = results[0];
    var response = results[1];
    res.send(data);
  })
  .catch(function (err) {
    logger.error('Error getting resource: ' + resource);
    logger.error(err.data);
    res.send(err.data || err);
  });
};

module.exports.getUpdateData = function(req, res, next) {
  var d = new Date();
  client.requestResource('/friends/leaderboard.json', 'GET', req.user.token,
    req.user.tokenSecret, req.user.id)
  .then(function (results) {
    var data = JSON.parse(results[0]);
    var response = results[1];
    var updateData = {};

    data.friends.forEach(function (friend) {
      var id = friend.user.encodedId;
      var lastUpdate = friend.lastUpdateTime;
      updateData[id] = lastUpdate;
    });

    res.send(updateData);
  })
  .catch(function (err) {
    res.send(err.data || err);
  });
};
