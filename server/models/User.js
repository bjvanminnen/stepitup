var Q = require('q');
var db = require('../db');

var logger = require('../logger');

var TABLE_NAME = 'Users';
var COLS = [
  'id',
  'token',
  'tokenSecret',
  'lastModified'
];

var User = function (props) {
  props = props || {};
  COLS.forEach(function (col) {
    this[col] = props[col] || null;
  }, this);
  this.id = this.id || null;

  this.persisted_ = null;
};
module.exports = User;

// TODO - feels ugly
function quoteAndJoin(list, quote) {
  return list.map(function (item) {
    return quote + item + quote;
  }).join(',');
}
/**
 * Save User to the db.
 * @returns a Q promise
 */
User.prototype.save = function () {
  var defer = Q.defer();
  if (process.env.NO_DB) {
    logger.warn('no db');
    defer.resolve(null);
    return defer.promise;
  }

  // TODO - validate data before saving
  // TODO - use some sort of query builder?

  if (this.persisted_) {
    var noChanges = true;
    for (var item in this.persisted_) {
      if (item === 'persisted_' || item === 'lastModified') {
        continue;
      }
      if (this.persisted_[item] !== this[item]) {
        noChanges = false;
      }
    }
    if (noChanges) {
      defer.resolve(this);
      return defer.promise;
    }
  }

  var str;
  if (this.persisted_) {
    var cols = quoteAndJoin(COLS.slice(1,4), '"');
    str = 'UPDATE ' + TABLE_NAME + ' SET(' + cols + ') = (' +
      quoteAndJoin([this.token, this.tokenSecret], "'") + ', DEFAULT) ' +
      ' WHERE id=\'' + this.id + '\' RETURNING *;';
  } else {
    var cols = quoteAndJoin(COLS.slice(0,3), '"');
    str = 'INSERT INTO ' + TABLE_NAME + ' (' + cols  + ') VALUES (' +
      quoteAndJoin([this.id, this.token, this.tokenSecret], "'") + ') RETURNING *;';
  }

  logger.db(str);
  db.performQueryQ(str)
  .then(function (result) {
    var row = result.rows[0];
    COLS.forEach(function(col) {
      this[col] = row[col];
    }, this);
    this.persisted_ = JSON.parse(JSON.stringify(this));
    defer.resolve(this);
  }.bind(this))
  .fail(function (err) {
    defer.reject(err);
  });

  return defer.promise;
};

/**
 * Find a user by id
 * @returns a Q promise with the found user
 */
User.find = function(id) {
  var defer = Q.defer();
  if (id === undefined) {
    defer.reject(new Error('no id defined'));
  }

  var str = 'SELECT * FROM ' + TABLE_NAME + ' WHERE id=\'' + id + '\' LIMIT 1;';
  db.performQueryQ(str)
  .then(function (result) {
    if (result.rows.length === 0) {
      // TODO test for this code path
      throw new Error('not found');
    }
    var user = new User(result.rows[0]);
    // TODO - is this too hacky of a way to clone?
    user.persisted_ = JSON.parse(JSON.stringify(user));
    defer.resolve(user);
  })
  .fail(function (err) {
    defer.reject(err);
  });

  return defer.promise;
};
