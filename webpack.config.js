module.exports = {
  entry: './client/src/jsx/main.jsx',
  output: {
    path: __dirname + '/client/build',
    filename: 'source_dbg.js'
  },
  module: {
    loaders: [
    {
      test: /\.jsx$/,
      // loader: 'jsx-loader?insertPragma=React.DOM&harmony'
      loader: 'jsx-loader?insertPragma=React.DOM'
    }
    ],
  },
  externals: {
    //don't bundle the 'react' npm package with our bundle.js
    //but get it from a global 'React' variable
    'react': 'React'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
