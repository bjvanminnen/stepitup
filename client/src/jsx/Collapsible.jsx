/** @jsx React.DOM */
'use strict'

var React = require('react');
var SignedTD = require('./SignedTD');

var inlineStyle = {
  display: 'inline'
};

var Collapsible = React.createClass({
  getInitialState: function () {
    return {
      collapsed: false
    };
  },

  handleClick: function () {
    console.log('click');
    this.setState({ collapsed: !this.state.collapsed });
  },

  render: function () {
    var iconClass = this.state.collapsed ?
      "fa fa-plus-square-o" : "fa fa-minus-square-o";

    var displayStyle = {
      display: this.state.collapsed ? 'none' : 'inline'
    };

    return (
      <div>
        <span className={iconClass} onClick={this.handleClick}></span>
        <h3 style={inlineStyle}> {this.props.title}</h3>
        <span style={displayStyle}>
          {this.props.children}
        </span>
      </div>
    );
  }
});

module.exports = Collapsible;
