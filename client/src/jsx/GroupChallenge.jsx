/** @jsx React.DOM */
'use strict'

var React = require('react')
var SignedTD = require('./SignedTD');
var GroupChallengeRow = require('./GroupChallengeRow');
var Collapsible = require('./Collapsible');

var inlineStyle = {
  display: 'inline'
};

module.exports = React.createClass({
  // TODO - does this belong on the client?
  calculateMultiplier: function () {
    // now in PST
    var now = moment.utc().subtract(8, 'hours');
    var start = moment.utc(this.props.startDate);

    return now.diff(start) / moment.duration(1, 'weeks');
  },

  render: function () {
    var allSteps = 0;
    var allPars = 0;
    var data = this.props.data;
    var updateData = this.props.updateData || {};
    var rows = [];

    var multiplier = this.calculateMultiplier();

    _.keys(data).forEach(function (userId, index) {
      var userData = data[userId];
      var userSteps = 0;
      var userPar = 0;
      _.keys(userData.daily).forEach(function (dateId, index) {
        userSteps += userData.daily[dateId].steps;
        userPar += userData.daily[dateId].processed.target;
      });
      rows.push(<GroupChallengeRow
        key={userId}
        name={userData.name}
        steps={userSteps}
        target={Math.round(userPar * multiplier)}
        diff={Math.round(userPar * multiplier) - userSteps}
        par={userPar}
        lastUpdate={updateData[userId]}
      />);

      allSteps += userSteps;
      allPars += userPar;
    });

    rows = _.sortBy(rows, function (row) {
      return data[row.key].name;
    });

    var currentTarget = Math.round(multiplier * allPars);
    var diff = (allSteps - currentTarget);
    var syncClass = this.props.updateData ? '' : 'hide';

    //todo - show rules while loading

    return (
      <div>
        <br/>
        <Collapsible title="Rules">
          <ul className='ruleList'>
            <li>All steps are worth 1 point.</li>
            <li>Scoring is from Monday to Sunday.</li>
            <li>At the beginning of the week, each person's average is calculated
            from the previous three weeks.</li>
            <li>The goal is for us as a group to make it into the positive.</li>
          </ul>
        </Collapsible>

        <Collapsible title="Points">
          <table id='GroupChallengeTable' className='challengeTable'>
            <thead>
              <tr>
                <td></td>
                <td>Steps</td>
                <td>Current Target</td>
                <td>Diff</td>
                <td>Weekly Target</td>
                <td className={syncClass}>Last Synced</td>
              </tr>
            </thead>
            <tr className='groupSum'>
              <td></td>
              <td>{allSteps.toLocaleString()}</td>
              <td>{currentTarget.toLocaleString()}</td>
              <SignedTD value={diff} background={true}/>
              <td>{allPars.toLocaleString()}</td>
              <td className={syncClass}></td>
            </tr>
            <tr></tr>
            {rows}
          </table>
          <div>&nbsp;</div>
        </Collapsible>

      </div>
    );
  }
});
