/** @jsx React.DOM */
'use strict'

var React = require('react')

var SummaryTable = require('./SummaryTable');
var DetailTable = require('./DetailTable');
var IndividualChallenge = require('./IndividualChallenge');
var GroupChallenge = require('./GroupChallenge');
var ChatSubmission = require('./ChatSubmission');
var Collapsible = require('./Collapsible');

var Page = React.createClass({
  // TODO - add propTypes

  getInitialState: function () {
    var today = moment();
    var sinceMonday = (today.day() - moment().day('Monday').day() + 7) % 7;
    var startDate = today.subtract(sinceMonday, 'days').format('YYYY-MM-DD');

    return {
      data: null,
      error: 'Loading...',
      startDate: startDate,
      name: null,
      updateData: null,
      type: null
    };
  },
  componentDidMount: function () {
    this.getData();
    this.getName();
    this.getUpdateData();
  },

  getData: function () {
    var startDate = this.state.startDate;
    var groupId = this.props.groupId;
    var url = '/steps/group/' + groupId + '/' + startDate;
    $.getJSON(url, function (results) {
      this.setState({data: results, error: null});
    }.bind(this))
    .fail(function (err) {
      console.log('err: ' + err);
    });
  },

  getUpdateData: function () {
    if (!$.cookie('userId')) {
      return;
    }

    // TODO - try to make it so that it doesnt look so much like the last
    // col gets tacked on seconds later
    $.getJSON('/fitbit/lastUpdate', function (results) {
      this.setState({updateData: results});
    }.bind(this))
    .fail(function (err) {
      console.log('err: ' + err);
    });;
  },

  getName: function () {
    if (!$.cookie('userId')) {
      return;
    }
    // TODO - this logic belong elsewhere? name might belong in cookie
    $.getJSON('/fitbit/profile', function (results) {
      if (results.errors) {
        console.log('/fitbit/profile err: ' + results.errors[0].message);
      }
      if (results.user && results.user.displayName) {
        this.setState({name: results.user.displayName });
      }
    }.bind(this));
  },

  handleDateSelectionChange: function (evt) {
    this.setState({
      data:null,
      startDate: evt.target.value}
    , function () {
      this.getData();
    }.bind(this));
  },

  setType: function (type) {
    this.setState({ type: type });
  },

  render: function () {
    var self;
    var tables = (<div>{this.state.error}</div>);
    var type = this.state.type;

    if (type === null) {
      type = this.props.defaultType || 'individual';
    }

    if (this.state.data) {
      if (type ==='group') {
        tables = <GroupChallenge
          data={this.state.data}
          startDate={this.state.startDate}
          updateData={this.state.updateData}/>
      } else  {
        tables = <IndividualChallenge
          data={this.state.data}
          startDate={this.state.startDate}
          updateData={this.state.updateData}/>
      }
    }

    var dateOptions = [];
    var currentDate = moment('2014-11-10');
    var now = moment();
    while (now.isAfter(currentDate)) {
      var dateString = currentDate.format('YYYY-MM-DD');
      var pretty = currentDate.format('MM/DD/YYYY');
      dateOptions.push(<option key={dateString} value={dateString}>{pretty}</option>);
      currentDate.add(1, 'weeks');
    }

    var welcome = null;
    if (this.state.name) {
      welcome = <div>Welcome {this.state.name}</div>;
    } else {
      welcome = <a href='/fitbit/auth'>Sign In</a>;
    }

    var boldStyle = { fontWeight: 'bold' };

    // style={type === 'group' ? boldStyle : null}
    // style={type === 'individual' ? boldStyle : null}

    return (
      <div>
        {welcome}
        <div>&nbsp;</div>
        <div>
          <span
            className={type === 'group' ? 'metro' : 'metro unselected'}
            onClick={this.setType.bind(this, 'group')}>
              Group
          </span>
          <span> | </span>
          <span
          className={type === 'individual' ? 'metro' : 'metro unselected'}
            onClick={this.setType.bind(this, 'individual')}>
              Individual
          </span>
        </div>
        <h3>Week starting:</h3>
        <select value={this.state.startDate} onChange={this.handleDateSelectionChange}>
          {dateOptions}
        </select>
        {tables}
        <Collapsible title="Chat">
          <ChatSubmission/>
        </Collapsible>
      </div>
    );
  }
});
module.exports = Page;
