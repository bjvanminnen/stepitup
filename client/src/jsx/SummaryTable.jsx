/** @jsx React.DOM */
'use strict'

var React = require('react');
var SummaryTableRow = require('./SummaryTableRow');
var Collapsible = require('./Collapsible');

var tableStyle = {
  border: '1px solid black'
};

var SummaryTable = React.createClass({
  handleUserChange: function (userId) {
    this.props.handleUserChange(userId);
  },

  render: function () {
    var self = this;
    var data = this.props.data;
    var updateData = this.props.updateData || {};

    var syncClass = this.props.updateData ? '' : 'hide';

    var rows = _.keys(data).map(function (userId) {
      var userData = data[userId];
      var steps = 0;
      var points = 0;
      var pointsEOD = 0;
      var par = 0;

      Object.keys(userData.daily).forEach(function (dateId) {
        var dailyData = userData.daily[dateId];
        steps += dailyData.steps;
        points += dailyData.processed.points;
        pointsEOD += dailyData.processed.EODPoints;
        par += dailyData.processed.target;
      });

      return <SummaryTableRow
        key={userId}
        handleClick={self.handleUserChange.bind(self, userId)}
        name={userData.name}
        points={points}
        pointsEOD={pointsEOD}
        steps={steps}
        par={par}
        lastUpdate={updateData[userId]}/>
    });
    rows = _.sortBy(rows, function (row) {
      return row.props.points;
    }).reverse();

    return (
      <Collapsible title="Points">
        <div>Click a name for a daily breakdown.</div>
        <table className='challengeTable'>
        <thead>
          <tr>
            <td>Name</td>
            <td>Points</td>
            <td>EOD Points</td>
            <td>Steps</td>
            <td>Par</td>
            <td className={syncClass}>Last Synced</td>
          </tr>
        </thead>
          {rows}
        </table>
        <div>&nbsp;</div>
      </Collapsible>
    );
  }
});
module.exports = SummaryTable;
