/** @jsx React.DOM */
'use strict'

var React = require('react')

var SummaryTable = require('./SummaryTable');
var DetailTable = require('./DetailTable');
var Collapsible = require('./Collapsible');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      userId: null
    };
  },

  handleUserChange: function (userId) {
    this.setState({userId: userId});
  },

  render: function () {
    return (
      <div>
        <div>
          <br/>
          <Collapsible title="Rules">
            <ul className='ruleList'>
              <li>All steps are worth 1 point.</li>
              <li>Scoring is from Monday to Sunday.</li>
              <li>At the beginning of the week, your daily average is calculated
              from the previous three weeks.</li>
              <li>You start each day with a negative number of points equal to
              your average</li>
            </ul>
          </Collapsible>
        </div>

        <SummaryTable
          data={this.props.data}
          startDate={this.props.startDate}
          handleUserChange={this.handleUserChange}
          updateData={this.props.updateData}/>

        {this.state.userId ?
          <DetailTable
            data={this.props.data}
            userId={this.state.userId}
            startDate={this.props.startDate}/>
          : null
        }
      </div>
    );
  }
});
