/** @jsx React.DOM */
'use strict'

var React = require('react')
var SignedTD = require('./SignedTD');
var LastUpdateTD = require('./LastUpdateTD');


var SummaryTableRow = React.createClass({
  render: function () {
    return (
      <tr onClick={this.props.handleClick}>
        <td >
          {this.props.name}
        </td>
        <SignedTD value={this.props.points}/>
        <SignedTD value={this.props.pointsEOD}/>
        <td>
          {this.props.steps.toLocaleString()}
        </td>
        <td>
          {this.props.par.toLocaleString()}
        </td>
        <LastUpdateTD lastUpdate={this.props.lastUpdate}/>
      </tr>
    );
  }
});
module.exports = SummaryTableRow;
