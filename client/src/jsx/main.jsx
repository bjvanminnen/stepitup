var Page = require('./Page');
var React = require('react');

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {
  var type = getParameterByName('type') || '';

  React.render(
    <Page groupId='1' defaultType={type}/>,
    document.getElementById('content')
  );
});
