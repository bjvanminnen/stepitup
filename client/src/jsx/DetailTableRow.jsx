/** @jsx React.DOM */
'use strict'

var React = require('react');
var SignedTD = require('./SignedTD');

var DetailTableRow = React.createClass({
  render: function () {
    return (
      <tr>
        <td >
          {this.props.date}
        </td>
        <SignedTD value={this.props.points}/>
        <td>
          {this.props.steps.toLocaleString()}
        </td>
      </tr>
    );
  }
});
module.exports = DetailTableRow;
