/** @jsx React.DOM */
'use strict'

var React = require('react');

var SignedTD = React.createClass({
  render: function () {
    var color = this.props.value >= 0 ? 'limegreen' : 'red';
    var type = this.props.background ? 'background' : 'color';
    var style = {};
    style[type] = color;

    return (
      <td style={style}>
        {this.props.value.toLocaleString()}
      </td>
    );
  }
});
module.exports = SignedTD;
