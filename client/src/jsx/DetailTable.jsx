/** @jsx React.DOM */
'use strict'

var React = require('react');
var DetailTableRow = require('./DetailTableRow');

var DetailTable = React.createClass({
  render: function () {
    var data = this.props.data;
    var userData = data[this.props.userId];

    var headers = ['Day', 'Points', 'Steps'];
    headers = headers.map(function (h) {
      return (<td key={h}>{h}</td>);
    });

    var rows = [];
    var date = moment.utc(this.props.startDate);
    for (var i = 0; i < 7; i++) {
      var dateKey = date.format('YYYY-MM-DD');
      rows.push(<DetailTableRow
        key={dateKey}
        date={date.format('MM-DD-YYYY')}
        points={userData.daily[dateKey].processed.points}
        steps={userData.daily[dateKey].steps}/>
      );
      date.add(1, 'days');
    }

    return (<div>
      <table className='challengeTable'>
        <thead>
        <tr><td>{userData.name}</td></tr>
        <tr>{headers}</tr>
        </thead>
        {rows}
      </table>
    </div>);
  }
});
module.exports = DetailTable;
