var SignedTD = require('./SignedTD');
var LastUpdateTD = require('./LastUpdateTD');

module.exports = React.createClass({
  render: function () {
    return (
      <tr>
        <td>{this.props.name}</td>
        <td>{this.props.steps.toLocaleString()}</td>
        <td>{this.props.target.toLocaleString()}</td>
        <SignedTD value={this.props.diff}/>
        <td>{this.props.par.toLocaleString()}</td>
        <LastUpdateTD lastUpdate={this.props.lastUpdate}/>
      </tr>
    );
  }

});
