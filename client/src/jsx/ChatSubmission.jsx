/** @jsx React.DOM */
'use strict'

var React = require('react');

var THIRTY_MIN = 1000 * 60 * 30;

function friendlyDate(unfriendly) {
  if (!unfriendly) {
    return '';
  }
  var d = new Date(unfriendly);
  var hours = d.getHours();
  var half = (hours >= 12) ? 'pm' : 'am';
  if (hours === 0) {
    hours = 12
  } else if (hours > 12) {
    hours -= 12;
  }

  var minutes = d.getMinutes();
  if (minutes < 10) {
    minutes = '0' + minutes;
  }
  return hours + ':' + minutes + half;
}

var ChatSubmission = React.createClass({
  messageInterval: 3000,

  getInitialState: function () {
    return {
      messages: []
    };
  },

  getMessages: function () {
    var self = this;
    $.get('/chat', function (results) {
      self.messageInterval = 3000; // reset on success
      self.setState({messages: results});
    }).fail(function () {
      console.log('error getting db (%s)', self.messageInterval);
      // slow down message rate
      self.messageInterval = Math.min(self.messageInterval * 1.5, THIRTY_MIN);
    });
    setTimeout(function () {
      self.getMessages();
    }, this.messageInterval);
  },

  handleSubmit: function (evt) {
    evt.preventDefault();
    var element = evt.target.children[0];
    $.post( "/chat/add", { msg: element.value });

    var msg = {
      timestamp: new Date(),
      message: element.value
    };

    var newMessages = [msg].concat(this.state.messages);
    element.value = '';
    this.getMessages();
    this.setState({messages: newMessages});
  },

  componentDidMount: function () {
    this.getMessages();
  },

  render: function () {

    var messages = this.state.messages.map(function (msg, index) {
      return (
        <div key={index}>
          <span className='chatTimestamp'>{friendlyDate(msg.timestamp)}: </span>
          <span>{msg.message}</span>
        </div>);
    });

    return (<div>
      <form onSubmit={this.handleSubmit}>
        <input className='chatBox' type="text"/>
      </form>
      <div className='chatResults'>
        {messages}
      </div>
    </div>);
  }
});
module.exports = ChatSubmission;
