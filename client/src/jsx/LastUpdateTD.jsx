/** @jsx React.DOM */
'use strict'

var React = require('react');

var LastUpdateTD = React.createClass({
  render: function () {
    var now = moment();
    var lastUpdate = moment(this.props.lastUpdate);
    var lastUpdateStr;
    if (lastUpdate.date() === now.date() &&
        now.diff(lastUpdate) < moment.duration(1, 'days')) {
      lastUpdateStr = lastUpdate.format('h:mm a');
    } else {
      lastUpdateStr = lastUpdate.format('MM/DD h:mm a');
    }

    return (
      <td className={this.props.lastUpdate ? '' : 'hide'}>
        {lastUpdateStr}
      </td>
    );
  }
});
module.exports = LastUpdateTD;
