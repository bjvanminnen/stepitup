/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var Page = __webpack_require__(2);
	var React = __webpack_require__(1);

	function getParameterByName(name) {
	  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	  results = regex.exec(location.search);
	  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	$(document).ready(function () {
	  var type = getParameterByName('type') || '';

	  React.render(
	    React.createElement(Page, {groupId: "1", defaultType: type}),
	    document.getElementById('content')
	  );
	});


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = React;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1)

	var SummaryTable = __webpack_require__(3);
	var DetailTable = __webpack_require__(4);
	var IndividualChallenge = __webpack_require__(5);
	var GroupChallenge = __webpack_require__(6);
	var ChatSubmission = __webpack_require__(7);
	var Collapsible = __webpack_require__(8);

	var Page = React.createClass({displayName: 'Page',
	  // TODO - add propTypes

	  getInitialState: function () {
	    var today = moment();
	    var sinceMonday = (today.day() - moment().day('Monday').day() + 7) % 7;
	    var startDate = today.subtract(sinceMonday, 'days').format('YYYY-MM-DD');

	    return {
	      data: null,
	      error: 'Loading...',
	      startDate: startDate,
	      name: null,
	      updateData: null,
	      type: null
	    };
	  },
	  componentDidMount: function () {
	    this.getData();
	    this.getName();
	    this.getUpdateData();
	  },

	  getData: function () {
	    var startDate = this.state.startDate;
	    var groupId = this.props.groupId;
	    var url = '/steps/group/' + groupId + '/' + startDate;
	    $.getJSON(url, function (results) {
	      this.setState({data: results, error: null});
	    }.bind(this))
	    .fail(function (err) {
	      console.log('err: ' + err);
	    });
	  },

	  getUpdateData: function () {
	    if (!$.cookie('userId')) {
	      return;
	    }

	    // TODO - try to make it so that it doesnt look so much like the last
	    // col gets tacked on seconds later
	    $.getJSON('/fitbit/lastUpdate', function (results) {
	      this.setState({updateData: results});
	    }.bind(this))
	    .fail(function (err) {
	      console.log('err: ' + err);
	    });;
	  },

	  getName: function () {
	    if (!$.cookie('userId')) {
	      return;
	    }
	    // TODO - this logic belong elsewhere? name might belong in cookie
	    $.getJSON('/fitbit/profile', function (results) {
	      if (results.errors) {
	        console.log('/fitbit/profile err: ' + results.errors[0].message);
	      }
	      if (results.user && results.user.displayName) {
	        this.setState({name: results.user.displayName });
	      }
	    }.bind(this));
	  },

	  handleDateSelectionChange: function (evt) {
	    this.setState({
	      data:null,
	      startDate: evt.target.value}
	    , function () {
	      this.getData();
	    }.bind(this));
	  },

	  setType: function (type) {
	    this.setState({ type: type });
	  },

	  render: function () {
	    var self;
	    var tables = (React.createElement("div", null, this.state.error));
	    var type = this.state.type;

	    if (type === null) {
	      type = this.props.defaultType || 'individual';
	    }

	    if (this.state.data) {
	      if (type ==='group') {
	        tables = React.createElement(GroupChallenge, {
	          data: this.state.data, 
	          startDate: this.state.startDate, 
	          updateData: this.state.updateData})
	      } else  {
	        tables = React.createElement(IndividualChallenge, {
	          data: this.state.data, 
	          startDate: this.state.startDate, 
	          updateData: this.state.updateData})
	      }
	    }

	    var dateOptions = [];
	    var currentDate = moment('2014-11-10');
	    var now = moment();
	    while (now.isAfter(currentDate)) {
	      var dateString = currentDate.format('YYYY-MM-DD');
	      var pretty = currentDate.format('MM/DD/YYYY');
	      dateOptions.push(React.createElement("option", {key: dateString, value: dateString}, pretty));
	      currentDate.add(1, 'weeks');
	    }

	    var welcome = null;
	    if (this.state.name) {
	      welcome = React.createElement("div", null, "Welcome ", this.state.name);
	    } else {
	      welcome = React.createElement("a", {href: "/fitbit/auth"}, "Sign In");
	    }

	    var boldStyle = { fontWeight: 'bold' };

	    // style={type === 'group' ? boldStyle : null}
	    // style={type === 'individual' ? boldStyle : null}

	    return (
	      React.createElement("div", null, 
	        welcome, 
	        React.createElement("div", null, " "), 
	        React.createElement("div", null, 
	          React.createElement("span", {
	            className: type === 'group' ? 'metro' : 'metro unselected', 
	            onClick: this.setType.bind(this, 'group')}, 
	              "Group"
	          ), 
	          React.createElement("span", null, " | "), 
	          React.createElement("span", {
	          className: type === 'individual' ? 'metro' : 'metro unselected', 
	            onClick: this.setType.bind(this, 'individual')}, 
	              "Individual"
	          )
	        ), 
	        React.createElement("h3", null, "Week starting:"), 
	        React.createElement("select", {value: this.state.startDate, onChange: this.handleDateSelectionChange}, 
	          dateOptions
	        ), 
	        tables, 
	        React.createElement(Collapsible, {title: "Chat"}, 
	          React.createElement(ChatSubmission, null)
	        )
	      )
	    );
	  }
	});
	module.exports = Page;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);
	var SummaryTableRow = __webpack_require__(9);
	var Collapsible = __webpack_require__(8);

	var tableStyle = {
	  border: '1px solid black'
	};

	var SummaryTable = React.createClass({displayName: 'SummaryTable',
	  handleUserChange: function (userId) {
	    this.props.handleUserChange(userId);
	  },

	  render: function () {
	    var self = this;
	    var data = this.props.data;
	    var updateData = this.props.updateData || {};

	    var syncClass = this.props.updateData ? '' : 'hide';

	    var rows = _.keys(data).map(function (userId) {
	      var userData = data[userId];
	      var steps = 0;
	      var points = 0;
	      var pointsEOD = 0;
	      var par = 0;

	      Object.keys(userData.daily).forEach(function (dateId) {
	        var dailyData = userData.daily[dateId];
	        steps += dailyData.steps;
	        points += dailyData.processed.points;
	        pointsEOD += dailyData.processed.EODPoints;
	        par += dailyData.processed.target;
	      });

	      return React.createElement(SummaryTableRow, {
	        key: userId, 
	        handleClick: self.handleUserChange.bind(self, userId), 
	        name: userData.name, 
	        points: points, 
	        pointsEOD: pointsEOD, 
	        steps: steps, 
	        par: par, 
	        lastUpdate: updateData[userId]})
	    });
	    rows = _.sortBy(rows, function (row) {
	      return row.props.points;
	    }).reverse();

	    return (
	      React.createElement(Collapsible, {title: "Points"}, 
	        React.createElement("div", null, "Click a name for a daily breakdown."), 
	        React.createElement("table", {className: "challengeTable"}, 
	        React.createElement("thead", null, 
	          React.createElement("tr", null, 
	            React.createElement("td", null, "Name"), 
	            React.createElement("td", null, "Points"), 
	            React.createElement("td", null, "EOD Points"), 
	            React.createElement("td", null, "Steps"), 
	            React.createElement("td", null, "Par"), 
	            React.createElement("td", {className: syncClass}, "Last Synced")
	          )
	        ), 
	          rows
	        ), 
	        React.createElement("div", null, " ")
	      )
	    );
	  }
	});
	module.exports = SummaryTable;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);
	var DetailTableRow = __webpack_require__(10);

	var DetailTable = React.createClass({displayName: 'DetailTable',
	  render: function () {
	    var data = this.props.data;
	    var userData = data[this.props.userId];

	    var headers = ['Day', 'Points', 'Steps'];
	    headers = headers.map(function (h) {
	      return (React.createElement("td", {key: h}, h));
	    });

	    var rows = [];
	    var date = moment.utc(this.props.startDate);
	    for (var i = 0; i < 7; i++) {
	      var dateKey = date.format('YYYY-MM-DD');
	      rows.push(React.createElement(DetailTableRow, {
	        key: dateKey, 
	        date: date.format('MM-DD-YYYY'), 
	        points: userData.daily[dateKey].processed.points, 
	        steps: userData.daily[dateKey].steps})
	      );
	      date.add(1, 'days');
	    }

	    return (React.createElement("div", null, 
	      React.createElement("table", {className: "challengeTable"}, 
	        React.createElement("thead", null, 
	        React.createElement("tr", null, React.createElement("td", null, userData.name)), 
	        React.createElement("tr", null, headers)
	        ), 
	        rows
	      )
	    ));
	  }
	});
	module.exports = DetailTable;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1)

	var SummaryTable = __webpack_require__(3);
	var DetailTable = __webpack_require__(4);
	var Collapsible = __webpack_require__(8);

	module.exports = React.createClass({displayName: 'exports',
	  getInitialState: function () {
	    return {
	      userId: null
	    };
	  },

	  handleUserChange: function (userId) {
	    this.setState({userId: userId});
	  },

	  render: function () {
	    return (
	      React.createElement("div", null, 
	        React.createElement("div", null, 
	          React.createElement("br", null), 
	          React.createElement(Collapsible, {title: "Rules"}, 
	            React.createElement("ul", {className: "ruleList"}, 
	              React.createElement("li", null, "All steps are worth 1 point."), 
	              React.createElement("li", null, "Scoring is from Monday to Sunday."), 
	              React.createElement("li", null, "At the beginning of the week, your daily average is calculated" + ' ' +
	              "from the previous three weeks."), 
	              React.createElement("li", null, "You start each day with a negative number of points equal to" + ' ' +
	              "your average")
	            )
	          )
	        ), 

	        React.createElement(SummaryTable, {
	          data: this.props.data, 
	          startDate: this.props.startDate, 
	          handleUserChange: this.handleUserChange, 
	          updateData: this.props.updateData}), 

	        this.state.userId ?
	          React.createElement(DetailTable, {
	            data: this.props.data, 
	            userId: this.state.userId, 
	            startDate: this.props.startDate})
	          : null
	        
	      )
	    );
	  }
	});


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1)
	var SignedTD = __webpack_require__(11);
	var GroupChallengeRow = __webpack_require__(12);
	var Collapsible = __webpack_require__(8);

	var inlineStyle = {
	  display: 'inline'
	};

	module.exports = React.createClass({displayName: 'exports',
	  // TODO - does this belong on the client?
	  calculateMultiplier: function () {
	    // now in PST
	    var now = moment.utc().subtract(8, 'hours');
	    var start = moment.utc(this.props.startDate);

	    return now.diff(start) / moment.duration(1, 'weeks');
	  },

	  render: function () {
	    var allSteps = 0;
	    var allPars = 0;
	    var data = this.props.data;
	    var updateData = this.props.updateData || {};
	    var rows = [];

	    var multiplier = this.calculateMultiplier();

	    _.keys(data).forEach(function (userId, index) {
	      var userData = data[userId];
	      var userSteps = 0;
	      var userPar = 0;
	      _.keys(userData.daily).forEach(function (dateId, index) {
	        userSteps += userData.daily[dateId].steps;
	        userPar += userData.daily[dateId].processed.target;
	      });
	      rows.push(React.createElement(GroupChallengeRow, {
	        key: userId, 
	        name: userData.name, 
	        steps: userSteps, 
	        target: Math.round(userPar * multiplier), 
	        diff: Math.round(userPar * multiplier) - userSteps, 
	        par: userPar, 
	        lastUpdate: updateData[userId]}
	      ));

	      allSteps += userSteps;
	      allPars += userPar;
	    });

	    rows = _.sortBy(rows, function (row) {
	      return data[row.key].name;
	    });

	    var currentTarget = Math.round(multiplier * allPars);
	    var diff = (allSteps - currentTarget);
	    var syncClass = this.props.updateData ? '' : 'hide';

	    //todo - show rules while loading

	    return (
	      React.createElement("div", null, 
	        React.createElement("br", null), 
	        React.createElement(Collapsible, {title: "Rules"}, 
	          React.createElement("ul", {className: "ruleList"}, 
	            React.createElement("li", null, "All steps are worth 1 point."), 
	            React.createElement("li", null, "Scoring is from Monday to Sunday."), 
	            React.createElement("li", null, "At the beginning of the week, each person's average is calculated" + ' ' +
	            "from the previous three weeks."), 
	            React.createElement("li", null, "The goal is for us as a group to make it into the positive.")
	          )
	        ), 

	        React.createElement(Collapsible, {title: "Points"}, 
	          React.createElement("table", {id: "GroupChallengeTable", className: "challengeTable"}, 
	            React.createElement("thead", null, 
	              React.createElement("tr", null, 
	                React.createElement("td", null), 
	                React.createElement("td", null, "Steps"), 
	                React.createElement("td", null, "Current Target"), 
	                React.createElement("td", null, "Diff"), 
	                React.createElement("td", null, "Weekly Target"), 
	                React.createElement("td", {className: syncClass}, "Last Synced")
	              )
	            ), 
	            React.createElement("tr", {className: "groupSum"}, 
	              React.createElement("td", null), 
	              React.createElement("td", null, allSteps.toLocaleString()), 
	              React.createElement("td", null, currentTarget.toLocaleString()), 
	              React.createElement(SignedTD, {value: diff, background: true}), 
	              React.createElement("td", null, allPars.toLocaleString()), 
	              React.createElement("td", {className: syncClass})
	            ), 
	            React.createElement("tr", null), 
	            rows
	          ), 
	          React.createElement("div", null, " ")
	        )

	      )
	    );
	  }
	});


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);

	var THIRTY_MIN = 1000 * 60 * 30;

	function friendlyDate(unfriendly) {
	  if (!unfriendly) {
	    return '';
	  }
	  var d = new Date(unfriendly);
	  var hours = d.getHours();
	  var half = (hours >= 12) ? 'pm' : 'am';
	  if (hours === 0) {
	    hours = 12
	  } else if (hours > 12) {
	    hours -= 12;
	  }

	  var minutes = d.getMinutes();
	  if (minutes < 10) {
	    minutes = '0' + minutes;
	  }
	  return hours + ':' + minutes + half;
	}

	var ChatSubmission = React.createClass({displayName: 'ChatSubmission',
	  messageInterval: 3000,

	  getInitialState: function () {
	    return {
	      messages: []
	    };
	  },

	  getMessages: function () {
	    var self = this;
	    $.get('/chat', function (results) {
	      self.messageInterval = 3000; // reset on success
	      self.setState({messages: results});
	    }).fail(function () {
	      console.log('error getting db (%s)', self.messageInterval);
	      // slow down message rate
	      self.messageInterval = Math.min(self.messageInterval * 1.5, THIRTY_MIN);
	    });
	    setTimeout(function () {
	      self.getMessages();
	    }, this.messageInterval);
	  },

	  handleSubmit: function (evt) {
	    evt.preventDefault();
	    var element = evt.target.children[0];
	    $.post( "/chat/add", { msg: element.value });

	    var msg = {
	      timestamp: new Date(),
	      message: element.value
	    };

	    var newMessages = [msg].concat(this.state.messages);
	    element.value = '';
	    this.getMessages();
	    this.setState({messages: newMessages});
	  },

	  componentDidMount: function () {
	    this.getMessages();
	  },

	  render: function () {

	    var messages = this.state.messages.map(function (msg, index) {
	      return (
	        React.createElement("div", {key: index}, 
	          React.createElement("span", {className: "chatTimestamp"}, friendlyDate(msg.timestamp), ": "), 
	          React.createElement("span", null, msg.message)
	        ));
	    });

	    return (React.createElement("div", null, 
	      React.createElement("form", {onSubmit: this.handleSubmit}, 
	        React.createElement("input", {className: "chatBox", type: "text"})
	      ), 
	      React.createElement("div", {className: "chatResults"}, 
	        messages
	      )
	    ));
	  }
	});
	module.exports = ChatSubmission;


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);
	var SignedTD = __webpack_require__(11);

	var inlineStyle = {
	  display: 'inline'
	};

	var Collapsible = React.createClass({displayName: 'Collapsible',
	  getInitialState: function () {
	    return {
	      collapsed: false
	    };
	  },

	  handleClick: function () {
	    console.log('click');
	    this.setState({ collapsed: !this.state.collapsed });
	  },

	  render: function () {
	    var iconClass = this.state.collapsed ?
	      "fa fa-plus-square-o" : "fa fa-minus-square-o";

	    var displayStyle = {
	      display: this.state.collapsed ? 'none' : 'inline'
	    };

	    return (
	      React.createElement("div", null, 
	        React.createElement("span", {className: iconClass, onClick: this.handleClick}), 
	        React.createElement("h3", {style: inlineStyle}, " ", this.props.title), 
	        React.createElement("span", {style: displayStyle}, 
	          this.props.children
	        )
	      )
	    );
	  }
	});

	module.exports = Collapsible;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1)
	var SignedTD = __webpack_require__(11);
	var LastUpdateTD = __webpack_require__(13);


	var SummaryTableRow = React.createClass({displayName: 'SummaryTableRow',
	  render: function () {
	    return (
	      React.createElement("tr", {onClick: this.props.handleClick}, 
	        React.createElement("td", null, 
	          this.props.name
	        ), 
	        React.createElement(SignedTD, {value: this.props.points}), 
	        React.createElement(SignedTD, {value: this.props.pointsEOD}), 
	        React.createElement("td", null, 
	          this.props.steps.toLocaleString()
	        ), 
	        React.createElement("td", null, 
	          this.props.par.toLocaleString()
	        ), 
	        React.createElement(LastUpdateTD, {lastUpdate: this.props.lastUpdate})
	      )
	    );
	  }
	});
	module.exports = SummaryTableRow;


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);
	var SignedTD = __webpack_require__(11);

	var DetailTableRow = React.createClass({displayName: 'DetailTableRow',
	  render: function () {
	    return (
	      React.createElement("tr", null, 
	        React.createElement("td", null, 
	          this.props.date
	        ), 
	        React.createElement(SignedTD, {value: this.props.points}), 
	        React.createElement("td", null, 
	          this.props.steps.toLocaleString()
	        )
	      )
	    );
	  }
	});
	module.exports = DetailTableRow;


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);

	var SignedTD = React.createClass({displayName: 'SignedTD',
	  render: function () {
	    var color = this.props.value >= 0 ? 'limegreen' : 'red';
	    var type = this.props.background ? 'background' : 'color';
	    var style = {};
	    style[type] = color;

	    return (
	      React.createElement("td", {style: style}, 
	        this.props.value.toLocaleString()
	      )
	    );
	  }
	});
	module.exports = SignedTD;


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var SignedTD = __webpack_require__(11);
	var LastUpdateTD = __webpack_require__(13);

	module.exports = React.createClass({displayName: 'exports',
	  render: function () {
	    return (
	      React.createElement("tr", null, 
	        React.createElement("td", null, this.props.name), 
	        React.createElement("td", null, this.props.steps.toLocaleString()), 
	        React.createElement("td", null, this.props.target.toLocaleString()), 
	        React.createElement(SignedTD, {value: this.props.diff}), 
	        React.createElement("td", null, this.props.par.toLocaleString()), 
	        React.createElement(LastUpdateTD, {lastUpdate: this.props.lastUpdate})
	      )
	    );
	  }

	});


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM *//** @jsx React.DOM */
	'use strict'

	var React = __webpack_require__(1);

	var LastUpdateTD = React.createClass({displayName: 'LastUpdateTD',
	  render: function () {
	    var now = moment();
	    var lastUpdate = moment(this.props.lastUpdate);
	    var lastUpdateStr;
	    if (lastUpdate.date() === now.date() &&
	        now.diff(lastUpdate) < moment.duration(1, 'days')) {
	      lastUpdateStr = lastUpdate.format('h:mm a');
	    } else {
	      lastUpdateStr = lastUpdate.format('MM/DD h:mm a');
	    }

	    return (
	      React.createElement("td", {className: this.props.lastUpdate ? '' : 'hide'}, 
	        lastUpdateStr
	      )
	    );
	  }
	});
	module.exports = LastUpdateTD;


/***/ }
/******/ ])